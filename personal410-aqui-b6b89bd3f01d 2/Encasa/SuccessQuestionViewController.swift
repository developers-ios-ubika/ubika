//
//  SuccessQuestionViewController.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 26/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SuccessQuestionViewController: UIViewController{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    var dni: String!
    var pass: String!
    var tyc: String!
    var tycOto: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        addStyleToElements()
    }
    
    //MARK: - Private methods
    private func initView() {
        titleLabel.text = "Validación de identidad satisfactoria"
        subTitleLabel.text = "Tu cuenta ha sido creada con éxito."
    }

    private func addStyleToElements(){

        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = Constants.Colors.grayTextValidation
        
        titleLabel.setStyleFont(sizeFont: 18, withBold: true)

        subTitleLabel.numberOfLines = 0
        subTitleLabel.textAlignment = .center
        subTitleLabel.textColor = Constants.Colors.grayTextValidation
        
        subTitleLabel.setStyleFont(sizeFont: 15, withBold: false)
        
        nextButton.personalizeButtonWithBorder(with: "Continuar", sizeFont: 14, withBold: true)
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func netButtonPressed(_ sender: UIButton) {
        createAccountPt2()
    }
    
    private func createAccountPt2(){

        self.showActivityIndicator(uiView: self.view)
        self.view.isUserInteractionEnabled = false
        
        let request = RegisterCreateRequest(Usuario: self.dni, ClaveInicial: pass, AceTyC: tyc, OtoCon: tycOto, TipoContr: 2, OrigenAplicacion: 11, VerPoli: "1", VerDatPer: "1")

        OriginData.sharedInstance.createAccountPt2( request: request){ (data) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            
            if data.code != "0"{
                self.newAlert(titulo: "Login", mensaje: data.mensaje ?? "", nameButton: "Terminar")
                return
            }
            
            self.iniValidarLogin()
            
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func iniValidarLogin(){
        
        let parametros = [
            "Usuario" : self.dni,
            "UsuCla" : self.pass,
            "Origen" : "S",
            "Originado" : "IOS"
        ]

        OriginData.sharedInstance.validarNewLogin(parametros: parametros as NSDictionary) { (user) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            GlobalVariables.sharedManager.infoUsuarioBean = user
            self.getLoginDetail()
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func getLoginDetail() {
        
        var ValorCodigoWS = "0"
        
        if let user = GlobalVariables.sharedManager.infoUsuarioBean{
            ValorCodigoWS = user.CodigoWs ?? "0"
        }else{
            ValorCodigoWS = "99"
        }
        
        if(ValorCodigoWS == "0"  || ValorCodigoWS == "2"){
            self .iniObtenerLoginDetalle()
        }else{
            self.obtenerMensajeError(codigo: ValorCodigoWS)
            
        }
    }
    
    func iniObtenerLoginDetalle(){
        
        self.showActivityIndicator(uiView: self.view)
        
        let parametros = [
            "Usuario" : self.dni,
            "SesionId" : GlobalVariables.sharedManager.infoUsuarioBean.UseSeIDSession ?? "" as String
        ]
        
        OriginData.sharedInstance.obtenerNewLoginDetalle(parametros: parametros as NSDictionary) { (data) in
            self.hideActivityIndicator(uiView: self.view)
            
            if data.codigoValidacion == "0"{
                GlobalVariables.sharedManager.infoUsuarioBean.SDT_Usuario = data.SDT_Usuario
                GlobalVariables.sharedManager.infoUsuarioBean.UseNroDoc = data.SDT_Usuario?.UseNroDoc
                GlobalVariables.sharedManager.infoUsuarioBean.UseFlgBonif = data.SDT_Usuario?.UseFlgBonif
                GlobalVariables.sharedManager.infoUsuarioBean.UseApePat = data.SDT_Usuario?.UseApePat
                GlobalVariables.sharedManager.infoUsuarioBean.UseNomCo = data.SDT_Usuario?.UseNomCo
                GlobalVariables.sharedManager.infoUsuarioBean.UseClave = data.SDT_Usuario?.UseClave
                GlobalVariables.sharedManager.infoUsuarioBean.UseApeMat = data.SDT_Usuario?.UseApeMat
                GlobalVariables.sharedManager.infoUsuarioBean.UseMail = data.SDT_Usuario?.UseMail
                GlobalVariables.sharedManager.infoUsuarioBean.UseNom = data.SDT_Usuario?.UseNom
                GlobalVariables.sharedManager.infoUsuarioBean.UseEst = data.SDT_Usuario?.UseEst
                GlobalVariables.sharedManager.infoUsuarioBean.UseTip = data.SDT_Usuario?.UseTip
                GlobalVariables.sharedManager.infoUsuarioBean.UseCod = data.SDT_Usuario?.UseCod
                GlobalVariables.sharedManager.infoUsuarioBean.UseKey = data.SDT_Usuario?.UseKey
                GlobalVariables.sharedManager.infoUsuarioBean.UsePerfil = data.SDT_Usuario?.UsePerfil
                GlobalVariables.sharedManager.infoUsuarioBean.UsuCla = self.pass
                GlobalVariables.sharedManager.infoUsuarioBean.UseNroDoc = self.dni
                GlobalVariables.sharedManager.infoUsuarioBean.Usuario = self.dni
                self.iniSetearToken()
            }
            
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    func iniSetearToken(){
        
        self.showActivityIndicator(uiView: self.view)

        var deviceToken = AccessUserDefaults.getUserDefaults(key: defaultsKeys.DEVICE_TOKEN_KEY)
        if deviceToken == "" {
            deviceToken = "51c95169c753136bfa0367f28112f477354584dbcf88a8f44c9e6a1f5fbe90e2"
        }
        print("deviceToken: \(deviceToken), y el usuario \(GlobalVariables.sharedManager.infoUsuarioBean ?? User())")
        
        let parametros = [
            "usuario" : self.dni,
            "sesionId" : GlobalVariables.sharedManager.infoUsuarioBean.UseSeIDSession! as String,
            "tokenId" : deviceToken,
            "so" : "IOS",
            "appId" : "4"
        ]
        
        OriginData.sharedInstance.setearNewToken(parametros: parametros as NSDictionary) { (push) in
            let notiPush = push as? NotificationPush
            if (notiPush?.result == "OK")
            {
                self.loginLoad()
            }
            else
            {
                self.showAlert(titulo: Constants.aplicacionNombre, mensaje: notiPush?.error ?? "")
            }
        }
    }
    
    func loginLoad(){
    
        
        let ValorCodigoWS = GlobalVariables.sharedManager.infoUsuarioBean.CodigoWs

        switch ValorCodigoWS {
            
        case "0":
            if(GlobalVariables.sharedManager.infoUsuarioBean.UseFlgClave == "S"){
                if !(GlobalVariables.sharedManager.infoUsuarioBean.UseFlgContratoCG == "S"){

                    let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                    let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ActualizarDNIVC") as UIViewController
                    self.present(vc, animated: false, completion: nil)
                    
                }else{
                    // Store user data
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(nil, forKey: defaultsKeys.USER_NAME_KEY)
                    userDefaults.set(nil, forKey: defaultsKeys.USER_PASSWORD_KEY)
                    userDefaults.synchronize()
                    self.irHome()
                    
                }
            }else if (GlobalVariables.sharedManager.infoUsuarioBean.UseFlgClave == "N"){
            
                GlobalVariables.sharedManager.infoUsuarioBean.tipoAccesoCambiarContrasenia = "L"
                
                let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "CambiarContrasenaVC") as UIViewController
                self.present(vc, animated: false, completion: nil)
            }
        case "2":
            
            GlobalVariables.sharedManager.infoUsuarioBean.tipoAccesoCambiarContrasenia = "L"
            
            let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "CambiarContrasenaVC") as UIViewController
            self.present(vc, animated: false, completion: nil)
            
            
            
        default:
            print("otro")
        }
    }
    
    func irHome(){
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let menuLeftViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuLeftVC") as! MenuLeftViewController
        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
        menuLeftViewController.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        homeViewController.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        menuLeftViewController.setDefaultViewController(defaultViewController: homeViewController)
        let slideMenuController = ExSlideMenuController(mainViewController: homeViewController, leftMenuViewController: menuLeftViewController)
        
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        SlideMenuOptions.contentViewScale = 1
        
        self.navigationController!.pushViewController(slideMenuController, animated: true)
    }
}
