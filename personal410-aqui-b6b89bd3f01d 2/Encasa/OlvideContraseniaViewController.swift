//
//  OlvideContraseniaViewController.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 1/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class OlvideContraseniaViewController: UIViewController, UITextFieldDelegate {

   
    @IBOutlet weak var itxtDNI: UITextField!
    @IBOutlet weak var itxtCorreo: UITextField!
    let grayBorderColor = UIColor.fromHex(rgbValue: 0xEFEFF4).cgColor

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.itxtDNI.delegate = self
        self.itxtCorreo.delegate = self

        itxtDNI.setBottomBorder(color: grayBorderColor)
        itxtCorreo.setBottomBorder(color: grayBorderColor)
        
        self.hideKeyboardWhenTappedAround()
        self.addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Siguiente", style: UIBarButtonItem.Style.done, target: self, action: #selector(OlvideContraseniaViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.itxtDNI.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.itxtDNI.resignFirstResponder()
        self.textFieldShouldReturn(textField: itxtDNI)
    }
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField === itxtDNI) {
            itxtCorreo.becomeFirstResponder()
        } else if (textField === itxtCorreo) {
            itxtCorreo.resignFirstResponder()
        }
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func accionRetornar(sender: AnyObject) {
    
        self.dismiss(animated: false, completion: nil);
    }
    
     @IBAction func accionContinuar(sender: AnyObject) {
        
        if(itxtCorreo.text != "" && itxtDNI.text != ""){
        
            if(itxtDNI.text!.count == 8){
            
            self.iniSolicitarCambiarClave()
            
            }else{
            
                let alertController = UIAlertController(title: "Recuperar Contraseña", message:
                    "Debe ingresar los 8 números de su DNI", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
                
            }
            
        }else if(itxtDNI.text == ""){
        
         
            let alertController = UIAlertController(title: "Recuperar Contraseña", message:
                "Ingrese su número de DNI", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }else if(itxtDNI.text!.count != 8){
            
            
            let alertController = UIAlertController(title: "Recuperar Contraseña", message:
                "Debe ingresar los 8 números de su DNI", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }else if(itxtCorreo.text == ""){
            
            
            let alertController = UIAlertController(title: "Recuperar Contraseña", message:
                "Ingrese su correo electrónico.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func iniSolicitarCambiarClave(){

        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(OlvideContraseniaViewController.endSolicitarCambiarClave(notification:)), name:NSNotification.Name(rawValue: "endSolicitarCambiarClave"), object: nil)
 
        let parametros = [
            "UseCod" : itxtDNI.text! as String,
            "UseMail" : itxtCorreo.text! as String,
        ]
        
        OriginData.sharedInstance.solicitarCambiarClave(notificacion: "endSolicitarCambiarClave", parametros: parametros as NSDictionary)
    }
    
    @objc func endSolicitarCambiarClave(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)

        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let data = notification.object as! UsuarioBean;
        
        if(data.Flag == "U"){
            
            let alertController = UIAlertController(title: "Recuperar Contraseña", message:
                "El DNI no existe o el correo ingresado no es válido. Por favor intente nuevamente.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title:"Aceptar", style: .default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            
            self.regresarViewController()
            
        }
        
    }

    
    @IBAction func accionCancelar(sender: AnyObject) {
        self.dismiss(animated: false, completion: nil);
    }
    
    func regresarViewController(){
        
        let alert = UIAlertController(title: "Recuperar Contraseña", message: "En breve le llegará un correo con un link para cambiar su contraseña", preferredStyle: .alert)
        let action = UIAlertAction(title: "Aceptar", style: .default) { (action) -> Void in
            
            let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
            
            let viewControllerYouWantToPresent = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC")
            
            self.present(viewControllerYouWantToPresent, animated: false, completion: nil)
        }
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        
        

    }
}
