//
//  MiEmpresaTableViewCell.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 21/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class MiEmpresaTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nombreEmpresaLB: UILabel!
    @IBOutlet weak var numeroRucLB: UILabel!
    @IBOutlet weak var mesLB: UILabel!
    @IBOutlet weak var semaforoIV: UIImageView!
    @IBOutlet weak var viewDeudaLB: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        //contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(10, 10, 10, 10))
    }

}
