//
//  NSArray.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 6/27/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

extension NSArray {

    func replaceObjectAtIndex(index: Int, array: NSArray, object: AnyObject) -> NSArray
    {
        let mutableArray: NSMutableArray = array.mutableCopy() as! NSMutableArray
        mutableArray[index] = object;
        
        return NSArray(array: mutableArray.copy() as! [AnyObject])
    }
    
    func addAnyObject(array: NSArray, object: AnyObject) -> NSArray
    {
        let mutableArray: NSMutableArray = array.mutableCopy() as! NSMutableArray
        mutableArray.add(object)
        
        return NSArray(array: mutableArray.copy() as! [AnyObject])
    }
}
