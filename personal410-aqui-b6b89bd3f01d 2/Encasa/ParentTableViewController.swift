//
//  ParentTableViewController.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/23/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class ParentTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(ParentTableViewController.endInternetConnection(notification:)), name:NSNotification.Name(rawValue: "endInternetConnection"), object: nil)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ParentTableViewController.didTapTableView(tapGestureRecognizer:)))
        tapRecognizer.cancelsTouchesInView = false;
        self.tableView.addGestureRecognizer(tapRecognizer)
    }
    
    
    @objc func didTapTableView(tapGestureRecognizer: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.tableView.superview?.endEditing(true)
    }

    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

    // MARK: - Table view data source
/*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }
*/
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
    }
    
    @objc func endInternetConnection(notification: NSNotification){
    
        self.hideActivityIndicator(uiView: self.view)
        
        self.showAlertConnectivity()
    }
    
    func showAlertConnectivity(){
        
        let alerta = UIAlertController(title: Constants.aplicacionNombre, message: "Verifique su conexión a internet e inténtelo nuevamente.", preferredStyle: .alert)
        alerta.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { (action: UIAlertAction!) in
            
        }))
        
        self.present(alerta, animated: true, completion: nil)
    }
}
