//
//  ValidationIdentidadTableViewCell.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 25/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

protocol ValidationIdentidadDelegate: class{
    func check(cell: ValidationIdentidadTableViewCell, index: IndexPath)
}

class ValidationIdentidadTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var checkButton: UIButton!
    
    weak var delegate: ValidationIdentidadDelegate?
    
    var index : IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = Constants.Colors.grayTextValidation
        
        titleLabel.setStyleFont(sizeFont: 12, withBold: true)
        self.selectionStyle = .none
    }

    func loadData(title: String){
        titleLabel.text = title
    }
    
    func isLastView(isLast: Bool){
        self.lineView.alpha = isLast ? 0 : 1
    }
    
    func selectCheck(isSelect: Bool){
        checkButton.isSelected = isSelect
    }
    
    @IBAction func checkButtonPressed(_ sender: UIButton) {
        //sender.isSelected = !sender.isSelected
        self.delegate?.check(cell: self, index: self.index)
    }
}
