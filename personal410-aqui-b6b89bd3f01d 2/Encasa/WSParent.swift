//
//  WSParent.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/28/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import Alamofire

class WSParent {

    static func prepareSessionDataTask(notificacion: String, parametros: NSDictionary, metodo: String, clase: AnyClass, timeoutInterval: TimeInterval = 30){
        
        let configuration = URLSessionConfiguration .default
        configuration.timeoutIntervalForRequest = timeoutInterval
        configuration.timeoutIntervalForResource = timeoutInterval
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let method = metodo
        var urlString : NSString = NSString(format: "\(PATHS.listaservice)RWS_VD_listaWebService" as NSString)

        
        if method != "RWS_VD_listaWebService"{
            var listaBean:WebServiceList!
            listaBean = GlobalVariables.sharedManager.infoListaWebServices
            var codigoOk = 0
            if listaBean.result.count > 0 {
                 for i in 0 ... listaBean.result.count - 1 {
                
                    let objetoLista: WebServiceResult = listaBean.result[i]
                                if objetoLista.nombre as String == metodo{
                                    print("el logeo: \(objetoLista.nombre)")
                                    let changeURL = NSString(format: "\(objetoLista.url)" as NSString)
                                    let changedUrl = changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                                    urlString = NSString(format: "\(changedUrl)" as NSString)
                                    print(urlString)
                                    codigoOk = 1
                                }
                            }
                            if codigoOk == 0 {
                                let changeURL = NSString(format: "\(PATHS.webservice)\(method)" as NSString)
                                changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                                let changedUrl = changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                                urlString = NSString(format: "\(changedUrl)" as NSString)
                                print(urlString)
//                                urlString = NSString(format: "\(PATHS.webservice)\(method)" as NSString)
                            }
            }else{
                let changeURL = NSString(format: "\(PATHS.webservice)\(method)" as NSString)
                changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                let changedUrl = changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                urlString = NSString(format: "\(changedUrl)" as NSString)
                print(urlString)
            }

        }
        let request = NSMutableURLRequest()
        request.url = URL(string: NSString(format: "%@", urlString) as String)
        print(urlString)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.timeoutInterval = timeoutInterval

        do{
            request.httpBody  = try! JSONSerialization.data(withJSONObject: parametros, options: [])
            session.dataTask(with: request as URLRequest) { (data, response, error) in
                guard let httpResponse = response as? HTTPURLResponse, let receivedData = data
                    else {
                        //print("error: not a valid http response")
                        return
                }

                var objectBean = NSObject()

                switch (httpResponse.statusCode)
                {
                case 200:

                    var response = NSString (data: receivedData, encoding: String.Encoding.utf8.rawValue) as! String
                    print("response data: \(response)")

                    switch (clase){
                    case is UsuarioBean.Type:
                        //print("Lo necesito: \(UsuarioBean(jsonStr: response, clase: UsuarioBean.self))")
                        //response = (response as NSString).replacingOccurrences(of: "CodigoValidacion", with: "codigoValidacion")

                        //objectBean = UsuarioBean(jsonStr: response, clase: UsuarioBean.self)
//                        if urlString == "iniciarSesion"{
//                            do {
//                                let decodedData = try JSONDecoder().decode(
//                                    UserLoginResponse.self,
//                                    from: receivedData
//                                )
//                                //print("El usuario sin codear: \(response) usuario decodeado:\(decodedData)")
//                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: decodedData)
//                            } catch  {
//                                print("error codeando: \(error)")
//                            }
//                        }else {
                            do {
                                let decodedData = try JSONDecoder().decode(
                                    User.self,
                                    from: receivedData
                                )
                               // print("El usuario sin codear: \(response) usuario decodeado:\(decodedData)")
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: decodedData)
                            } catch  {
                                //print("error codeando: \(error)")
                            }
                        //}
                        
                    case is NotificationPush.Type:
                        let noti = NotificationPush(jsonStr: response, clase: NotificationPush.self)
                        do {
                            let decodedData = try JSONDecoder().decode(
                                Response.self,
                                from: receivedData
                            )
                           // print("El usuario sin codear: \(response) usuario decodeado:\(decodedData)")
                            noti.result = decodedData.result
                            noti.error = decodedData.error
                        } catch  {
                            //print("error codeando: \(error)")
                        }
                        objectBean = noti
                    case is ErrorBean.Type:
                        let errorBean = ErrorBean(jsonStr: response, clase: ErrorBean.self)
                        do {
                            let decodedData = try JSONDecoder().decode(
                                ErrorBeanCodable.self,
                                from: receivedData
                            )
                           // print("El usuario sin codear: \(response) usuario decodeado:\(decodedData)")
                            errorBean.ErrorWSDes = decodedData.ErrorWSDes
                        } catch  {
                            //print("error codeando: \(error)")
                        }
                        objectBean = ErrorBean(jsonStr: response, clase: ErrorBean.self)
                    case is DatoServicioBean.Type:
                        objectBean = DatoServicioBean(jsonStr: response, clase: DatoServicioBean.self)
                    case is SolicitudVerificacionBean.Type:
                        let solicitud: SolicitudVerificacionBean = SolicitudVerificacionBean(jsonStr: response, clase: SolicitudVerificacionBean.self)
                        do {
                                                       let decodedData = try JSONDecoder().decode(
                                                           SolicitudVerificacionResponse.self,
                                                           from: receivedData
                                                       )
                            let err: ErrorBean = ErrorBean()
                            if decodedData.error.count > 0 {
                                err.code = "\(decodedData.error[0].code)"
                                err.message = decodedData.error[0].message
                            }
                            solicitud.error.append(err)
                                                       //print("El notificacion decodeado:\(decodedData)")
//                                                       NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: decodedData)
                                                   } catch  {
                                                       print("error codeando: \(error)")
                                                   }
                        objectBean = solicitud
                    case is ResultadoBean.Type:
                        objectBean = ResultadoBean(jsonStr: response, clase: ResultadoBean.self)
                    case is ListaInicialBean.Type:
                        //print("Lo necesito: \(ListaInicialBean(jsonStr: response, clase: ListaInicialBean.self))")
                        //objectBean = ListaInicialBean(jsonStr: response, clase: ListaInicialBean.self)
                        do {
                            let decodedData = try JSONDecoder().decode(
                                WebServiceList.self,
                                from: receivedData
                            )
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: decodedData)
                        } catch  {
                            print("error codeando: \(error)")
                        }
                    default:
                        objectBean = NSObject(jsonStr: response, clase: NSObject.self)
                    }

                default:
                    print("save profile POST request got response \(httpResponse.statusCode), y descripcion \(urlString)")

                    switch (clase){
                    case is UsuarioBean.Type:
                        objectBean = UsuarioBean()
                    case is NotificationPush.Type:
                        objectBean = NotificationPush()
                    case is ErrorBean.Type:
                        objectBean = ErrorBean()
                    case is DatoServicioBean.Type:
                        objectBean = DatoServicioBean()
                    case is SolicitudVerificacionBean.Type:
                        objectBean = SolicitudVerificacionBean()
                    case is ResultadoBean.Type:
                        objectBean = ResultadoBean()
                    case is ListaInicialBean.Type:
                        objectBean = ListaInicialBean()
                    default:
                        objectBean = NSObject()
                    }

                }
                if clase is ListaInicialBean.Type || clase is UsuarioBean {
                    
                }else{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: objectBean)
                }
                
            }.resume()
        }
        catch
        {
            print("Ocurrio un error .....")
        }
    }
    
    
}


extension WSParent {
    static func getServiceList(notificacion: String, parametros: ServiceListRequest, metodo: String, timeoutInterval: TimeInterval = 30){
        
        AF.request("\(PATHS.listaservice)RWS_VD_listaWebService",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<WebServiceList, AFError>) in
            switch response.result {
            case .success(let webServices):
                print("af webServices: \(webServices)")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: webServices)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode)" ?? "0", message: "")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notificacion), object: err)
                   // print("af error: \(error.errorDescription)")
                }
            
            }
        }
    }
    
    static func validateLogin(notification: String, parametros: LoginRequest, metodo: String){
        let url = getMethodUrl(metodo: metodo)
        AF.request(url,method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<User, AFError>) in
            switch response.result {
            case .success(let logged):
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification), object: logged)
            case .failure(let error):
                let err = ErrorResponse(code: "0", message: "")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification), object: err)
                print("af error: \(error)")
            }
        }
    }
    
    static func getUser(notification: String, parametros: UserRequest, metodo: String){
        let url = getMethodUrl(metodo: metodo)
            AF.request(url,method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<User, AFError>) in
                switch response.result {
                case .success(let logged):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification), object: logged)
                case .failure(let error):
                    let err = ErrorResponse(code: "0", message: "")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification), object: err)
                    print("af error: \(error)")
                }
            
        }
    }
    
    static func getRequestsList(notification: String, parametros: SolicitudesRequest, metodo: String){
        let url = getMethodUrl(metodo: metodo)
            AF.request(url,method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<SolicitudVerificacionResponse, AFError>) in
                switch response.result {
                case .success(let logged):
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification), object: logged)
                case .failure(let error):
                    let err = ErrorResponse(code: "0", message: "")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: notification), object: err)
                    print("af error: \(error)")
                }
            
        }
    }
    
    static func getMethodUrl(metodo: String) -> String {
        let listaBean:WebServiceList! = GlobalVariables.sharedManager.infoListaWebServices
        var url  = ""
        for i in 0 ... listaBean.result.count - 1 {
        
            let objetoLista: WebServiceResult = listaBean.result[i]
                        if objetoLista.nombre as String == metodo{
                            let changedUrl = objetoLista.url.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                            url = changedUrl
                        }
                    }
        return url
    }
   
}

struct ServiceListRequest: Codable{
    
}

// Se agrega extension para separar el response del servicio actual
extension WSParent{
    
    static func getTyC(parametros: TermyCRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.TermyConResp, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSObtieneVersionTyC",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<RsptaTermyCResponse, AFError>) in
            switch response.result {
            case .success(let data):
                print("af webServices: \(data)")
                success(data)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func sendQuestionary(parametros: ValidateCuestionaryRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.RsptasQuestionary, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSValidaCuestionario",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<RsptaCuestionaryResponse, AFError>) in
            switch response.result {
            case .success(let data):
                print("af webServices: \(data)")
                success(data)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func getQuestionaries(parametros: RegisterGenCuestionaryRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.Questionaries, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSGeneraCuestionario",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<RegisterGenQuestionaryResponse, AFError>) in
            switch response.result {
            case .success(let data):
                print("af webServices: \(data)")
                success(data)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func createAccountPt2(parametros: RegisterCreateRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSPaso6Reg",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<MessageDataResponse, AFError>) in
            switch response.result {
            case .success(let data):
                print("af webServices: \(data)")
                success(data)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func createAccountPt1(parametros: RegisterCreateRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSPaso5Reg",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<MessageDataResponse, AFError>) in
            switch response.result {
            case .success(let data):
                print("af webServices: \(data)")
                success(data)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func sendConfirmSms(parametros: RegisterConfirmSmsRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSPaso4Reg",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<MessageDataResponse, AFError>) in
            switch response.result {
            case .success(let data):
                print("af webServices: \(data)")
                success(data)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func sendSms(parametros: RegisterSmsRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSPaso3Reg",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<MessageDataResponse, AFError>) in
            switch response.result {
            case .success(let data):
                print("af webServices: \(data)")
                success(data)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func registerData(parametros: RegisterDataRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSPaso2Reg",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<MessageDataResponse, AFError>) in
            switch response.result {
            case .success(let data):
                print("af webServices: \(data)")
                success(data)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func getNewDni(parametros: RegisterDniRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.RegisterDni, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.registerStepsqa)RWS_MSPaso1Reg",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<RegisterDniResponse, AFError>) in
            switch response.result {
            case .success(let dni):
                print("af webServices: \(dni)")
                success(dni)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            }
        }
    }
    
    static func getNewServiceList(parametros: ServiceListRequest, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.WebSList, andError errorMesg: @escaping Constants.ErrorMessage){
        
        AF.request("\(PATHS.listaservice)RWS_VD_listaWebService",method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<WebServiceList, AFError>) in
            switch response.result {
            case .success(let webServices):
                print("af webServices: \(webServices)")
                success(webServices)
            case .failure(let error):
                if error.errorDescription == "URLSessionTask failed with error: The request timed out." {

                    let err = ErrorResponse(code: "500", message: "The request timed out.")
                    errorMesg(err)
                }else {
                    let err = ErrorResponse(code: "\(error.responseCode ?? 0)", message: "")
                    errorMesg(err)
                }
            
            }
        }
    }
    
    static func validateNewLogin(parametros: LoginRequest, metodo: String, withSuccess success: @escaping Constants.UserResp, andError errorMesg: @escaping Constants.ErrorMessage){
        let url = getMethodUrl(metodo: metodo)
        AF.request(url,method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<User, AFError>) in
            switch response.result {
            case .success(let logged):
                success(logged)
                print(logged)
            case .failure(let error):
                let err = ErrorResponse(code: "0", message: "")
                errorMesg(err)
                print("af error: \(error)")
            }
        }
    }
    
    static func getNewUser(parametros: UserRequest, metodo: String, withSuccess success: @escaping Constants.UserResp, andError errorMesg: @escaping Constants.ErrorMessage){
        let url = getMethodUrl(metodo: metodo)
        AF.request(url,method: .post, parameters: parametros, encoder: JSONParameterEncoder.prettyPrinted).validate().responseDecodable { (response: DataResponse<User, AFError>) in
            switch response.result {
            case .success(let logged):
                success(logged)
            case .failure(let error):
                let err = ErrorResponse(code: "0", message: "")
                errorMesg(err)
                print("af error: \(error)")
            }
        }
    }
    
    static func prepareNewSessionDataTask(parametros: NSDictionary, metodo: String, clase: AnyClass, timeoutInterval: TimeInterval = 30, withSuccess success: @escaping Constants.AnyClassToken){
        
        let configuration = URLSessionConfiguration .default
        configuration.timeoutIntervalForRequest = timeoutInterval
        configuration.timeoutIntervalForResource = timeoutInterval
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: OperationQueue.main)
        let method = metodo
        var urlString : NSString = NSString(format: "\(PATHS.listaservice)RWS_VD_listaWebService" as NSString)

        
        if method != "RWS_VD_listaWebService"{
            var listaBean:WebServiceList!
            listaBean = GlobalVariables.sharedManager.infoListaWebServices
            var codigoOk = 0
            if listaBean.result.count > 0 {
                 for i in 0 ... listaBean.result.count - 1 {
                
                    let objetoLista: WebServiceResult = listaBean.result[i]
                                if objetoLista.nombre as String == metodo{
                                    print("el logeo: \(objetoLista.nombre)")
                                    let changeURL = NSString(format: "\(objetoLista.url)" as NSString)
                                    let changedUrl = changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                                    urlString = NSString(format: "\(changedUrl)" as NSString)
                                    print(urlString)
                                    codigoOk = 1
                                }
                            }
                            if codigoOk == 0 {
                                let changeURL = NSString(format: "\(PATHS.webservice)\(method)" as NSString)
                                changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                                let changedUrl = changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                                urlString = NSString(format: "\(changedUrl)" as NSString)
                                print(urlString)
                            }
            }else{
                let changeURL = NSString(format: "\(PATHS.webservice)\(method)" as NSString)
                changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                let changedUrl = changeURL.replacingOccurrences(of: "http://208.43.231.70/", with: "http://sentinelalerta.com/")
                urlString = NSString(format: "\(changedUrl)" as NSString)
                print(urlString)
            }

        }
        let request = NSMutableURLRequest()
        request.url = URL(string: NSString(format: "%@", urlString) as String)
        print(urlString)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.timeoutInterval = timeoutInterval

        do{
            request.httpBody  = try! JSONSerialization.data(withJSONObject: parametros, options: [])
            session.dataTask(with: request as URLRequest) { (data, response, error) in
                guard let httpResponse = response as? HTTPURLResponse, let receivedData = data
                    else {
                        return
                }

                var objectBean = NSObject()

                switch (httpResponse.statusCode)
                {
                case 200:

                    var response = NSString (data: receivedData, encoding: String.Encoding.utf8.rawValue)! as String
                    print("response data: \(response)")

                    switch (clase){
                    case is UsuarioBean.Type:

                            do {
                                let decodedData = try JSONDecoder().decode(
                                    User.self,
                                    from: receivedData
                                )
                                print("PASS DATA decodedData User")
                            } catch  {

                            }
                        
                    case is NotificationPush.Type:
                        let noti = NotificationPush(jsonStr: response, clase: NotificationPush.self)
                        do {
                            let decodedData = try JSONDecoder().decode(
                                Response.self,
                                from: receivedData
                            )
                            noti.result = decodedData.result
                            noti.error = decodedData.error
                        } catch  {
                        }
                        objectBean = noti
                    case is ErrorBean.Type:
                        let errorBean = ErrorBean(jsonStr: response, clase: ErrorBean.self)
                        do {
                            let decodedData = try JSONDecoder().decode(
                                ErrorBeanCodable.self,
                                from: receivedData
                            )
                            errorBean.ErrorWSDes = decodedData.ErrorWSDes
                        } catch  {
                        }
                        objectBean = ErrorBean(jsonStr: response, clase: ErrorBean.self)
                    case is DatoServicioBean.Type:
                        objectBean = DatoServicioBean(jsonStr: response, clase: DatoServicioBean.self)
                    case is SolicitudVerificacionBean.Type:
                        let solicitud: SolicitudVerificacionBean = SolicitudVerificacionBean(jsonStr: response, clase: SolicitudVerificacionBean.self)
                        do {
                                                       let decodedData = try JSONDecoder().decode(
                                                           SolicitudVerificacionResponse.self,
                                                           from: receivedData
                                                       )
                            let err: ErrorBean = ErrorBean()
                            if decodedData.error.count > 0 {
                                err.code = "\(decodedData.error[0].code)"
                                err.message = decodedData.error[0].message
                            }
                            solicitud.error.append(err)

                                                   } catch  {
                                                       print("error codeando: \(error)")
                                                   }
                        objectBean = solicitud
                    case is ResultadoBean.Type:
                        objectBean = ResultadoBean(jsonStr: response, clase: ResultadoBean.self)
                    case is ListaInicialBean.Type:
                        do {
                            let decodedData = try JSONDecoder().decode(
                                WebServiceList.self,
                                from: receivedData
                            )
                            
                            print("PASS DATA decodedData WebServiceList")
                        } catch  {
                            print("error codeando: \(error)")
                        }
                    default:
                        objectBean = NSObject(jsonStr: response, clase: NSObject.self)
                    }

                default:
                    print("save profile POST request got response \(httpResponse.statusCode), y descripcion \(urlString)")

                    switch (clase){
                    case is UsuarioBean.Type:
                        objectBean = UsuarioBean()
                    case is NotificationPush.Type:
                        objectBean = NotificationPush()
                    case is ErrorBean.Type:
                        objectBean = ErrorBean()
                    case is DatoServicioBean.Type:
                        objectBean = DatoServicioBean()
                    case is SolicitudVerificacionBean.Type:
                        objectBean = SolicitudVerificacionBean()
                    case is ResultadoBean.Type:
                        objectBean = ResultadoBean()
                    case is ListaInicialBean.Type:
                        objectBean = ListaInicialBean()
                    default:
                        objectBean = NSObject()
                    }

                }
                if clase is ListaInicialBean.Type || clase is UsuarioBean {
                    
                }else{
                    success(objectBean)
                }
                
            }.resume()
        }
        catch
        {
            print("Ocurrio un error .....")
        }
    }
}
