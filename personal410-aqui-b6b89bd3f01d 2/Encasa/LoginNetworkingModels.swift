//
//  LoginNetworkingModels.swift
//  Encasa
//
//  Created by orbita on 6/29/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import Foundation


struct LoginRequest: Codable {
    var Usuario: String
    var UsuCla: String
    var Origen: String
    var Originado: String
}

struct UserRequest: Codable {
    var Usuario: String
    var SesionId: String
}

struct SolicitudesRequest: Codable {
    var sesionId: String
    var plataforma: String
    var usuario: String
}
