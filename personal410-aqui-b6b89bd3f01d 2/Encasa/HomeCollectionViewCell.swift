//
//  HomeCollectionViewCell.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/22/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var iconoIV: UIImageView!
    
    @IBOutlet var tituloLB: UILabel!
    
    @IBOutlet weak var subtituloLB: UILabel!
    
    @IBOutlet weak var puntitoImg: UIImageView!
    
    @IBOutlet weak var fondoView: UIView!
    
    @IBOutlet weak var imageFoto: UIImageView!
    
    @IBOutlet weak var btnFoto: UIButton!
}
