//
//  FotoCollectionViewCell.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 6/27/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class FotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var tituloLB: UILabel!
    
    @IBOutlet weak var subtituloLB: UILabel!
    
    @IBOutlet weak var puntitoImg: UIImageView!
    
    @IBOutlet weak var fondoView: UIView!
    
    @IBOutlet weak var fondoSuperiorView: UIView!
    
    @IBOutlet weak var imageFoto: UIImageView!
}
