//
//  WSConsulta.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 13/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class WSConsulta: WSParent {
    
    static let sharedInstance = WSConsulta()


    static func consultaDatoServicioTercero(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWSDatosServicio", clase: DatoServicioBean.self)
    }
    
    static func consultaBonificacion(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWSDatosServicio", clase: DatoServicioBean.self)
    }
    

}
