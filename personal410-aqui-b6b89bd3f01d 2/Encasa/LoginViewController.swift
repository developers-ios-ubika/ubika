//
//  LoginViewController.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/21/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import TextFieldEffects
import CoreTelephony

struct defaultsKeys {
    static let DEVICE_TOKEN_KEY = "deviceTokenKey"
    static let USER_NAME_KEY = "userNameKey"
    static let USER_PASSWORD_KEY = "userPasswordKey"
    static let SOL_EMPRESA_TIPO_DOC = "empresaTipDoc"
    static let SOL_EMPRESA_NRO_DOC = "empresaNroDoc"
    static let SOL_CODIGO = "solicitudCodigo"
    static let SOL_EMPRESA_CODIGO = "solicitudCodigoEmp"
}

struct tipoEvento {
    static let EMPSOL = "EMPSOL" //Empezar Solicitud
    static let ESTDIR = "ESTDIR" //Establecer Dirección
    static let CONRUT = "CONRUT" //Controlar Ruta
    static let INIFOT = "INIFOT" //Iniciar Carga Fotos
    static let SUBFOT = "SUBFOT" //Subir Foto
    static let FINCFO = "FINCFO" //Fin de Subida de Fotos
    static let INIVER = "INIVER" //Iniciar Verificación
    static let FINVER = "FINVER" //Finalizar Verificación
    static let ALERT1 = "ALERT1" //Alerta 1
    static let ALERT2 = "ALERT2" //Alerta 2
    static let ALERT3 = "ALERT3" //Alerta 3
    static let FTIESP = "FTIESP" //Finalizo Tiempo Espera
    static let RTIESP = "RTIESP" //Reiniciar Tiempo Espera
    static let NRTESP = "NRTESP" //No Reiniciar Tiempo
    static let FUEPER = "FUEPER" //Fuera Perímetro
    static let RFUPER = "RFUPER" //Reiniciar Fuera Perímetro
    static let NRFPER = "NRFPER" //No Reiniciar Fuera Perímetro
}

struct nombreAudio {
    static let A1 = "audio1"
    static let A1y2 = "audio1y2"
    static let A3y4 = "audio3y4"
    static let A5 = "audio5"
    static let A6 = "audio6"
    static let A7y8 = "audio7y8"
    static let A9 = "audio9"
    static let A10y11 = "audio10y11"
    static let A12 = "audio12"
    static let A13 = "audio13"
    static let A14 = "audio14"
    static let A15 = "audio15"
    static let A16 = "audio16"
    static let A17y18 = "audio17y18"
    static let A19 = "audio19"
}

class LoginViewController: ParentTableViewController, UITextFieldDelegate {

    //MARK: VARIABLES
    
    @IBOutlet var usuarioTF: UITextField!
    @IBOutlet var contraseniaTF: UITextField!
    var cargarListaInicialBean : WebServiceList!
    var usuarioBean: User!
    var datoServicioBean:DatoServicioBean?
    
    //MARK: CICLO DE VIDA

    let grayBorderColor = UIColor.fromHex(rgbValue: 0xEFEFF4).cgColor

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.usuarioTF.delegate = self
        self.contraseniaTF.delegate = self
  
        self.usuarioTF.setBottomBorder(color: grayBorderColor)
        self.contraseniaTF.setBottomBorder(color: grayBorderColor)
        
        /*
        print("UIDevice.version().name  \(UIDevice.current.systemName)")
        print("UIDevice.version().version  \(UIDevice.current.systemVersion)")
        print("UIDevice.version().modelo  \(UIDevice.current.model)")
        print("UIDevice.version().name  \(UIDevice.current.name)")
        */
        self.availableSIM
        self.addDoneButtonOnKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//        
//        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
//            statusBar.backgroundColor = UIColor.fromHex(rgbValue: 0x0074FF)
//        }
        
        self.validarVersionIOS()
    
        //self.validarUsuario()
    }
    
    func validarVersionIOS() {
        
        let Device = UIDevice.current
        
        let tokenIOS : String = Device.systemVersion//("\(iosVersion)")
        
        print(tokenIOS)
        
        if ((tokenIOS == "11.0.0") || (tokenIOS == "11.0.1") || (tokenIOS == "11.0.2"))  {
            
            let alert = UIAlertController(title: "Aviso", message: "La versión iOS del dispositivo puede presentar algunos incovenientes con la aplicación. Te recomendamos actualizar a la versión más reciente.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                self.iniCargarLista()
            }))
            
            
            self.present(alert, animated: true, completion: nil)

        }
        else
        {
            self.iniCargarLista()
        }
    }
    
    
    func validarUsuario()
    {
        let userDefaults = UserDefaults.standard
        let userName = userDefaults.string(forKey: defaultsKeys.USER_NAME_KEY)
        let userPassword = userDefaults.string(forKey: defaultsKeys.USER_PASSWORD_KEY)
        
        if (userName != nil && userPassword != nil)
        {
            if (userName != "" && userPassword != "")
            {
                self.usuarioTF.text = userName
                self.contraseniaTF.text = userPassword
                
                self.iniValidarLogin()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Siguiente", style: UIBarButtonItem.Style.done, target: self, action: #selector(LoginViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.usuarioTF.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction()
    {
        self.usuarioTF.resignFirstResponder()
        self.textFieldShouldReturn(textField: usuarioTF)
    }
    
    override func textFieldShouldReturn(textField: UITextField) -> Bool {
        if (textField === usuarioTF) {
            contraseniaTF.becomeFirstResponder()
        } else if (textField === contraseniaTF) {
            contraseniaTF.resignFirstResponder()
        }
        return true
    }
    var availableSIM : Bool {
        if let cellularProvider  = CTTelephonyNetworkInfo().subscriberCellularProvider {
            if let mnCode = cellularProvider.mobileNetworkCode {
                print(mnCode)
                print(cellularProvider.carrierName)
                return true
            }
        }
        return false
    }
    func animateTextField(textField: UITextField, up: Bool) {
        let movementDistance:CGFloat = -130
        let movementDuration: Double = 0.3
        
        var movement:CGFloat = 0
        if up {
            movement = movementDistance
        }
        else {
            movement = -movementDistance
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    
    func textFieldDidBeginEditing(textField: UITextField) {
        self.animateTextField(textField: textField, up:true)
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        self.animateTextField(textField: textField, up:false)
    }
    //MARK: ACCIONES
    
    @IBAction func accionCrearCuenta(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ValidarDNIVC") as UIViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
        
    }

    @IBAction func accionOlvideContrasenia(sender: AnyObject) {
        
        let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "OlvideContraseniaVC") as UIViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
    }
    
    @IBAction func accionIngresar(sender: AnyObject) {
        
        if(usuarioTF.text != "" && contraseniaTF.text != ""){
            
            self.iniValidarLogin()
            
        }else if(usuarioTF.text == ""){
            
            let alertController = UIAlertController(title: "Login", message:
                "Ingrese su número de DNI.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }else if(contraseniaTF.text == ""){
            
            let alertController = UIAlertController(title: "Login", message:
                "Ingrese su contraseña.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Terminar", style: .cancel,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }
        
    }
    
    //MARK: FUNCIONES
    
    func iniCargarLista(){
        
        self.showActivityIndicator(uiView: self.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.endCargarLista(notification:)), name:NSNotification.Name(rawValue: "endCargarLista"), object: nil)
        
        let parametros = ["" : ""]
        
        OriginData.sharedInstance.cargaListaInicial(notificacion: "endCargarLista", parametros: parametros as NSDictionary)
    }
    
    @objc func endCargarLista(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        if ((notification.object as? ErrorResponse) == nil)
        {
            guard let list = notification.object as? WebServiceList else {
                iniCargarLista()
//                self.showAlert(titulo: "Alerta", mensaje: "Ocurrio un error inesperado intente mas tarde.")
                return
                
            }
            self.cargarListaInicialBean = list
            
            GlobalVariables.sharedManager.infoListaWebServices = self.cargarListaInicialBean
            //var prueba: ListaInicialBean = GlobalVariables.sharedManager.infoListaWebServices
            
            var error = self.cargarListaInicialBean.error
            
            
            if self.cargarListaInicialBean != nil {
                self.validarUsuario()
            }
            else
            {
                print(error[0])
            }
            
        }
        else
        {
            guard let error = notification.object as? ErrorResponse else {
            //                self.showAlert(titulo: "Alerta", mensaje: "Ocurrio un error inesperado intente mas tarde.")
                            return
                            
                        }
            print ("Error endCargarLista")
            self.showAlert(titulo: "\(error.code)", mensaje: error.message)
        }
    }

    
    func iniValidarLogin(){
    
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.endValidarLogin(notification:)), name:NSNotification.Name(rawValue: "endValidarLogin"), object: nil)
        
        let parametros = [
            "Usuario" : self.usuarioTF.text! as String,
            "UsuCla" : self.contraseniaTF.text! as String,
            "Origen" : "S",
            "Originado" : "IOS"
        ]
        
        OriginData.sharedInstance.validarLogin(notificacion: "endValidarLogin", parametros: parametros as NSDictionary)
        
    }
    
    @objc func endValidarLogin(notification: NSNotification){

        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        if (notification.object != nil)
        {
            guard let user = notification.object as? User else {
                self.showAlert(titulo: "Alerta", mensaje: "Ocurrio un error inesperado intente mas tarde.")
                return
                
            }
            self.usuarioBean = user
            
            GlobalVariables.sharedManager.infoUsuarioBean = self.usuarioBean
            
            var ValorCodigoWS = Int()
            
            if(usuarioBean != nil)
            {
                if(usuarioBean!.CodigoWs! != "")
                {
                    ValorCodigoWS = (usuarioBean!.CodigoWs! as NSString).integerValue
                }
            }
            else
            {
                ValorCodigoWS = 99
            }
            
            if(ValorCodigoWS == 0  || ValorCodigoWS == 2){
                print("cero \(usuarioBean)")
                
                self .iniObtenerLoginDetalle()
            }else{
                self.obtenerMensajeError(codigo: String(ValorCodigoWS))
                
            }
        }
        else
        {
            self.showAlertConnectivity()
        }
        
    }
    
    func iniObtenerLoginDetalle(){
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.endObtenerLoginDetalle(notification:)), name:NSNotification.Name(rawValue: "endObtenerLoginDetalle"), object: nil)
        
        let parametros = [
            "Usuario" : self.usuarioTF.text! as String,
            "SesionId" : self.usuarioBean!.UseSeIDSession! as String
        ]
        OriginData.sharedInstance.obtenerLoginDetalle(notificacion: "endObtenerLoginDetalle", parametros: parametros as NSDictionary)
        print(parametros)
    }
    
    @objc func endObtenerLoginDetalle(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let data = notification.object as! User;

        if(data.codigoValidacion == "0")
        {
            self.usuarioBean.SDT_Usuario = data.SDT_Usuario
            
            //self.usuarioBean = UsuarioBean()
            
GlobalVariables.sharedManager.infoUsuarioBean?.UseNroDoc = data.SDT_Usuario?.UseNroDoc
GlobalVariables.sharedManager.infoUsuarioBean?.UseFlgBonif = data.SDT_Usuario?.UseFlgBonif
GlobalVariables.sharedManager.infoUsuarioBean?.UseApePat = data.SDT_Usuario?.UseApePat
GlobalVariables.sharedManager.infoUsuarioBean?.UseNomCo = data.SDT_Usuario?.UseNomCo
GlobalVariables.sharedManager.infoUsuarioBean?.UseClave = data.SDT_Usuario?.UseClave
GlobalVariables.sharedManager.infoUsuarioBean?.UseApeMat = data.SDT_Usuario?.UseApeMat
GlobalVariables.sharedManager.infoUsuarioBean?.UseMail = data.SDT_Usuario?.UseMail
GlobalVariables.sharedManager.infoUsuarioBean?.UseNom = data.SDT_Usuario?.UseNom
GlobalVariables.sharedManager.infoUsuarioBean?.UseEst = data.SDT_Usuario?.UseEst
GlobalVariables.sharedManager.infoUsuarioBean?.UseTip = data.SDT_Usuario?.UseTip
GlobalVariables.sharedManager.infoUsuarioBean?.UseCod = data.SDT_Usuario?.UseCod
GlobalVariables.sharedManager.infoUsuarioBean?.UseKey = data.SDT_Usuario?.UseKey
GlobalVariables.sharedManager.infoUsuarioBean?.UsePerfil = data.SDT_Usuario?.UsePerfil
            
            self.usuarioBean.UsuCla = contraseniaTF.text
            self.usuarioBean.UseNroDoc = usuarioTF.text
            self.usuarioBean.Usuario = usuarioTF.text
            
            GlobalVariables.sharedManager.infoUsuarioBean = self.usuarioBean
            
            self.iniSetearToken()
            //self.iniConsultarServicio()
        }
        else
        {
            print("este es la alerta")
            self.AlertaMensaje(Title: "Login", Mensaje: "Problemas en la conexión, vuelva a intentar", Boton: "Terminar")
        }
    }
    
    func iniConsultarServicio(){
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.endConsultarServicio(notification:)), name:NSNotification.Name(rawValue: "endConsultarServicio"), object: nil)

        let parametros = [
            "Usuario" : self.usuarioBean!.UseNroDoc! as String,
            "SesionId" : self.usuarioBean!.UseSeIDSession! as String,
            "UseTipDoc" : self.usuarioBean!.UseTipDoc! as String,
            "UseCod" : self.usuarioBean!.UseNroDoc! as String,
            "FatProCod" : "14" as String
        ]
        
        OriginData.sharedInstance.consultarServicio(notificacion: "endConsultarServicio", parametros: parametros as NSDictionary)
    }
    
    @objc func endConsultarServicio(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let data = notification.object as! UsuarioBean;
        
        if(data.codigoValidacion=="0")
        {
            self.usuarioBean.AFSerCod = data.AFSerCod
        
            GlobalVariables.sharedManager.infoUsuarioBean = self.usuarioBean
            
            self.iniSetearToken()
        }
        else
        {
            self.obtenerMensajeError(codigo: data.codigoValidacion!)
        }
    }
    
//    func iniConsultarCantidadTercero(){
//        
//        
//        self.showActivityIndicator(uiView: self.view.superview!)
//        
//        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.endConsultarCantidadTercero(_:)), name:"endConsultarCantidadTercero", object: nil)
//        
//        let parametros = [
//            "FatProCod" : "14" as String,
//            "UserID" : usuarioBean!.UseNroDoc! as String,
//            "UseSeIDSession" : usuarioBean!.UseSeIDSession! as String,
//            "AFSerCod" : usuarioBean!.AFSerCod! as String,
//        ]
//
//    OriginData.sharedInstance.consultaDatosServicio("endConsultarCantidadTercero", parametros: parametros)
//    }
//    
//    func endConsultarCantidadTercero(notification: NSNotification){
//        
//        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
//        
//        self.hideActivityIndicator(uiView: self.view.superview!)
//        
//        let data = notification.object as! DatoServicioBean;
//        
//        if(data.codigoWs=="0"){
//
//            datoServicioBean = DatoServicioBean(dictionary: data.SDTDatosservicio)
//            
//            GlobalVariables.sharedManager.infoDatoServicioBean = datoServicioBean
//
//            self.iniSetearToken()
//            
//        }else{
//            
//            self.obtenerMensajeError(data.codigoWs!)
//            
//        }
    
        
//    }
    /*
    func iniSetearToken(){
        
        let epicToken = MCEpicToken()

        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.endSetearToken(_:)), name:"endSetearToken", object: nil)

        let valorEpicToken = epicToken.getToken()

        let parametros = [
            "Usuario" : self.usuarioTF.text! as String,
            "IdSesion" : usuarioBean!.UseSeIDSession! as String,
            "UseTokenApp" : valorEpicToken as String
        ]

        print("parametros: \(parametros)")
        
        OriginData.sharedInstance.setearToken("endSetearToken", parametros: parametros)
    }
    
    func endSetearToken(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view.superview!)
        
        let data = notification.object as! NotificationPush;
        
        //var CodigoWS :String!
        
        if(data.CodigoValidacion == ""){
   
            if(data.Existe == ""){
                if(data.Codigo == ""){
                    
                    
                }else{
                    //CodigoWS = data.Codigo
                }
            }else{
                //CodigoWS = data.Existe
            }
     
        }else{
            //CodigoWS = data.CodigoValidacion
        }      
       
        self.loginLoad()
    }
    */
    
    func iniSetearToken(){
        
        self.showActivityIndicator(uiView: self.view.superview!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.endSetearToken(notification:)), name:NSNotification.Name(rawValue: "endSetearToken"), object: nil)
        
        var deviceToken = AccessUserDefaults.getUserDefaults(key: defaultsKeys.DEVICE_TOKEN_KEY)
        if deviceToken == "" {
            deviceToken = "51c95169c753136bfa0367f28112f477354584dbcf88a8f44c9e6a1f5fbe90e2"
        }
        print("deviceToken: \(deviceToken), y el usuario \(GlobalVariables.sharedManager.infoUsuarioBean)")
        
        let parametros = [
            "usuario" : self.usuarioTF.text! as String,
            "sesionId" : self.usuarioBean!.UseSeIDSession! as String,
            "tokenId" : deviceToken,
            "so" : "IOS",
            "appId" : "4"
        ]
        
        OriginData.sharedInstance.setearToken(notificacion: "endSetearToken", parametros: parametros as NSDictionary)
//        self.loginLoad()
    }
    
    @objc func endSetearToken(notification: NSNotification){
        
        NotificationCenter.default.removeObserver(self, name: notification.name, object: nil)
        
        self.hideActivityIndicator(uiView: self.view)
        
        let data = notification.object as! NotificationPush
        
        if (data.result == "OK")
        {
            self.loginLoad()
        }
        else
        {
            self.showAlert(titulo: Constants.aplicacionNombre, mensaje: data.error)
        }
    }
    
    func loginLoad(){
    
        let ValorCodigoWS = (usuarioBean!.CodigoWs! as NSString).integerValue

        switch ValorCodigoWS {
            
        case 0:
            if(usuarioBean?.UseFlgClave == "S"){
                if !(usuarioBean?.UseFlgContratoCG == "S"){

                    let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                    let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ActualizarDNIVC") as UIViewController
                    self.present(vc, animated: false, completion: nil)
                    
                }else{
                    
                    // Store user data
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(usuarioBean?.Usuario, forKey: defaultsKeys.USER_NAME_KEY)
                    userDefaults.set(usuarioBean?.UsuCla, forKey: defaultsKeys.USER_PASSWORD_KEY)
                    userDefaults.synchronize()
                    
                    self.irHome()
                    
                }
            }else if (usuarioBean?.UseFlgClave == "N"){
            
                self.usuarioBean.tipoAccesoCambiarContrasenia = "L"
                
                GlobalVariables.sharedManager.infoUsuarioBean = self.usuarioBean
                
                let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "CambiarContrasenaVC") as UIViewController
                self.present(vc, animated: false, completion: nil)
            }
        case 2:
            
            self.usuarioBean.tipoAccesoCambiarContrasenia = "L"
            
            GlobalVariables.sharedManager.infoUsuarioBean = self.usuarioBean
            
            let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "CambiarContrasenaVC") as UIViewController
            self.present(vc, animated: false, completion: nil)
            
            
            
        default:
            print("otro")
        }
    }
    
    func AlertaMensaje (Title: String, Mensaje: String, Boton: String){
        
        let alertController = UIAlertController(title: Title, message:
            Mensaje, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Boton, style: .cancel,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        

    }
    
    func irHome(){
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let menuLeftViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuLeftVC") as! MenuLeftViewController
        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
        menuLeftViewController.usuarioBean = usuarioBean
        homeViewController.usuarioBean = usuarioBean
        menuLeftViewController.setDefaultViewController(defaultViewController: homeViewController)
        let slideMenuController = ExSlideMenuController(mainViewController: homeViewController, leftMenuViewController: menuLeftViewController)
        
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        SlideMenuOptions.contentViewScale = 1
        
        self.navigationController!.pushViewController(slideMenuController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var num : CGFloat = 0
        
        if traitCollection.horizontalSizeClass == .compact { // compact horizontal size class
            if traitCollection.verticalSizeClass == .compact { // compact vertical size class
                
                if(indexPath.row == 0){
                    
                    num = 220
                    
                }else if(indexPath.row == 1 || indexPath.row == 2){
                
                    num = 42
                    
                }else if(indexPath.row == 3){
                
                     num = 113
                
                }
                
            }
            else { // regular vertical size class
                
                if(indexPath.row == 0){
                    
                    num = 264
                    
                }else if(indexPath.row == 1 || indexPath.row == 2){
                    
                    num = 42
                    
                }else if(indexPath.row == 3){
                    
                    num = 189
                    
                }

            }
        } else { // regular size class
            
            if(indexPath.row == 0){
                
                num = 340
                
            }else if(indexPath.row == 1 || indexPath.row == 2){
                
                num = 62
                
            }else if(indexPath.row == 3){
                
                num = 189
                
            }
        }
        return num
    }
    
    //MARK: MÉTODOS DELEGADOS
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.backgroundColor = .clear
    }
}
