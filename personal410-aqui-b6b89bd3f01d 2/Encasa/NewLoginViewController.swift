//
//  NewLoginViewController.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 25/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import TextFieldEffects
import CoreTelephony

class NewLoginViewController: UIViewController{

    @IBOutlet weak var containerDniView: UIView!
    @IBOutlet weak var containerPassView: UIView!
    @IBOutlet weak var dniField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var noCloseSesion: UILabel!
    @IBOutlet weak var checkNoCloseSesionButton: UIButton!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        addStyleToElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.validarVersionIOS()
    }
    //MARK: - Private methods
    private func initView() {
        
        titleLabel.text = "¿Ya estás registrado?"
        subTitleLabel.text = "Inicia sesión aquí"
        noCloseSesion.text = "No cerrar sesión"
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(done))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func done(){
        view.endEditing(true)
    }
    
    private func addStyleToElements(){
        
        containerDniView.cutstomTextFieldsContainer(false)
        containerPassView.cutstomTextFieldsContainer(false)
        
        passTextField.isSecureTextEntry = true
        
        dniField.keyboardType = .numberPad
        passTextField.keyboardType = .default
        
        dniField.delegate = self
        passTextField.delegate = self
        
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = Constants.Colors.grayTextValidation
        
        subTitleLabel.numberOfLines = 0
        subTitleLabel.textAlignment = .center
        subTitleLabel.textColor = Constants.Colors.colorPrimary
        
        noCloseSesion.numberOfLines = 0
        noCloseSesion.textAlignment = .center
        noCloseSesion.textColor = Constants.Colors.grayTextValidation
        
        titleLabel.setStyleFont(sizeFont: 12, withBold: true)
        subTitleLabel.setStyleFont(sizeFont: 12, withBold: true)
        noCloseSesion.setStyleFont(sizeFont: 12, withBold: false)
        
        nextButton.personalizeButtonWithBorder(with: "Iniciar Sesión", sizeFont: 14, withBold: true)
        createButton.personalizeButtonGrayWithBorder(with: "Crear Cuenta", sizeFont: 14, withBold: true)
        
    }

    private func validateDni() -> Bool{
        if let text = self.dniField.text, text.trim().count > 0{
            let result = text.trim().count == 8
            result ? print("phone Success") : self.newAlert(titulo: "Login", mensaje: "Ingresa un número de 8 digitos", nameButton: "Terminar")
            return result
        }else{
            self.newAlert(titulo: "Login", mensaje: "Ingrese su número de DNI.", nameButton: "Terminar")
            return false
        }
    }
    
    private func validatePassword() -> Bool{
        if self.passTextField.hasText {
            return true
        }else{
            self.newAlert(titulo: "Login", mensaje: "Ingresa una contraseña", nameButton: "Terminar")
            return false
        }
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func checkButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func showPassButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passTextField.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func netButtonPressed(_ sender: UIButton) {
        if sender.tag == 0{
            if validateDni() && validatePassword(){
                self.iniValidarLogin()
            }
        }else{
            let destinationVC = UIStoryboard.register.instantiate(NewRegisterViewController.self)
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
    }
}

extension NewLoginViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if textField == dniField && count <= 8{
            return true
        }else if textField == passTextField{
            return true
        }else{
            return false
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == dniField && textField.hasText{
            containerDniView.cutstomTextFieldsContainer(true)
        }else if textField == dniField && !textField.hasText{
            containerDniView.cutstomTextFieldsContainer(false)
        }
        if textField == passTextField && textField.hasText{
            containerPassView.cutstomTextFieldsContainer(true)
        }else if textField == passTextField && !textField.hasText{
            containerPassView.cutstomTextFieldsContainer(false)
        }
    }
}

// Servicios y logicas actuales
extension NewLoginViewController{
    
    func validarVersionIOS() {
        
        let Device = UIDevice.current
        
        let tokenIOS : String = Device.systemVersion//("\(iosVersion)")
        
        print(tokenIOS)
        
        if ((tokenIOS == "11.0.0") || (tokenIOS == "11.0.1") || (tokenIOS == "11.0.2"))  {
            
            let alert = UIAlertController(title: "Aviso", message: "La versión iOS del dispositivo puede presentar algunos incovenientes con la aplicación. Te recomendamos actualizar a la versión más reciente.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { action in
                self.iniCargarLista()
            }))
            
            
            self.present(alert, animated: true, completion: nil)

        }
        else
        {
            self.iniCargarLista()
        }
    }
    
    func iniCargarLista(){
        
        self.showActivityIndicator(uiView: self.view)

        OriginData.sharedInstance.cargaNewListaInicial { (list) in
            
            self.hideActivityIndicator(uiView: self.view)
            GlobalVariables.sharedManager.infoListaWebServices = list
            self.validarUsuario()
            
        } andError: { (errorResponse) in
            
            self.hideActivityIndicator(uiView: self.view)
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
            
            
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    func validarUsuario()
    {
        let userDefaults = UserDefaults.standard
        let userName = userDefaults.string(forKey: defaultsKeys.USER_NAME_KEY)
        let userPassword = userDefaults.string(forKey: defaultsKeys.USER_PASSWORD_KEY)
        
        if let userName = userName, let userPassword = userPassword, userName.trim().count > 0 && userPassword.trim().count > 0{
            
            self.dniField.text = userName
            self.passTextField.text = userPassword
            self.checkNoCloseSesionButton.isSelected = true
            self.iniValidarLogin()
        }
        
    }
    
    func iniValidarLogin(){
        
        self.showActivityIndicator(uiView: self.view)
        
        let parametros = [
            "Usuario" : self.dniField.text! as String,
            "UsuCla" : self.passTextField.text! as String,
            "Origen" : "S",
            "Originado" : "IOS"
        ]

        OriginData.sharedInstance.validarNewLogin(parametros: parametros as NSDictionary) { (user) in
            self.hideActivityIndicator(uiView: self.view)
            
            GlobalVariables.sharedManager.infoUsuarioBean = user
            self.getLoginDetail()
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    func getLoginDetail() {
        
        var ValorCodigoWS = "0"
        
        if let user = GlobalVariables.sharedManager.infoUsuarioBean{
            ValorCodigoWS = user.CodigoWs ?? "0"
        }else{
            ValorCodigoWS = "99"
        }
        
        if(ValorCodigoWS == "0"  || ValorCodigoWS == "2"){
            self .iniObtenerLoginDetalle()
        }else{
            self.obtenerMensajeError(codigo: ValorCodigoWS)
            
        }
    }
    
    func iniObtenerLoginDetalle(){
        
        self.showActivityIndicator(uiView: self.view)
        
        let parametros = [
            "Usuario" : self.dniField.text! as String,
            "SesionId" : GlobalVariables.sharedManager.infoUsuarioBean.UseSeIDSession ?? "" as String
        ]
        
        OriginData.sharedInstance.obtenerNewLoginDetalle(parametros: parametros as NSDictionary) { (data) in
            self.hideActivityIndicator(uiView: self.view)
            
            if data.codigoValidacion == "0"{
                GlobalVariables.sharedManager.infoUsuarioBean.SDT_Usuario = data.SDT_Usuario
                GlobalVariables.sharedManager.infoUsuarioBean.UseNroDoc = data.SDT_Usuario?.UseNroDoc
                GlobalVariables.sharedManager.infoUsuarioBean.UseFlgBonif = data.SDT_Usuario?.UseFlgBonif
                GlobalVariables.sharedManager.infoUsuarioBean.UseApePat = data.SDT_Usuario?.UseApePat
                GlobalVariables.sharedManager.infoUsuarioBean.UseNomCo = data.SDT_Usuario?.UseNomCo
                GlobalVariables.sharedManager.infoUsuarioBean.UseClave = data.SDT_Usuario?.UseClave
                GlobalVariables.sharedManager.infoUsuarioBean.UseApeMat = data.SDT_Usuario?.UseApeMat
                GlobalVariables.sharedManager.infoUsuarioBean.UseMail = data.SDT_Usuario?.UseMail
                GlobalVariables.sharedManager.infoUsuarioBean.UseNom = data.SDT_Usuario?.UseNom
                GlobalVariables.sharedManager.infoUsuarioBean.UseEst = data.SDT_Usuario?.UseEst
                GlobalVariables.sharedManager.infoUsuarioBean.UseTip = data.SDT_Usuario?.UseTip
                GlobalVariables.sharedManager.infoUsuarioBean.UseCod = data.SDT_Usuario?.UseCod
                GlobalVariables.sharedManager.infoUsuarioBean.UseKey = data.SDT_Usuario?.UseKey
                GlobalVariables.sharedManager.infoUsuarioBean.UsePerfil = data.SDT_Usuario?.UsePerfil
                GlobalVariables.sharedManager.infoUsuarioBean.UsuCla = self.passTextField.text
                GlobalVariables.sharedManager.infoUsuarioBean.UseNroDoc = self.dniField.text
                GlobalVariables.sharedManager.infoUsuarioBean.Usuario = self.dniField.text
                self.iniSetearToken()
            }
            
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    func iniSetearToken(){
        
        self.showActivityIndicator(uiView: self.view)

        var deviceToken = AccessUserDefaults.getUserDefaults(key: defaultsKeys.DEVICE_TOKEN_KEY)
        if deviceToken == "" {
            deviceToken = "51c95169c753136bfa0367f28112f477354584dbcf88a8f44c9e6a1f5fbe90e2"
        }
        print("deviceToken: \(deviceToken), y el usuario \(GlobalVariables.sharedManager.infoUsuarioBean ?? User())")
        
        let parametros = [
            "usuario" : self.dniField.text! as String,
            "sesionId" : GlobalVariables.sharedManager.infoUsuarioBean.UseSeIDSession! as String,
            "tokenId" : deviceToken,
            "so" : "IOS",
            "appId" : "4"
        ]
        
        OriginData.sharedInstance.setearNewToken(parametros: parametros as NSDictionary) { (push) in
            let notiPush = push as? NotificationPush
            if (notiPush?.result == "OK")
            {
                self.loginLoad()
            }
            else
            {
                self.showAlert(titulo: Constants.aplicacionNombre, mensaje: notiPush?.error ?? "")
            }
        }
    }
    
    func loginLoad(){
    
        
        let ValorCodigoWS = GlobalVariables.sharedManager.infoUsuarioBean.CodigoWs

        switch ValorCodigoWS {
            
        case "0":
            if(GlobalVariables.sharedManager.infoUsuarioBean.UseFlgClave == "S"){
                if !(GlobalVariables.sharedManager.infoUsuarioBean.UseFlgContratoCG == "S"){

                    let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                    let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "ActualizarDNIVC") as UIViewController
                    self.present(vc, animated: false, completion: nil)
                    
                }else{
                    // Store user data
                    let userDefaults = UserDefaults.standard
                    if self.checkNoCloseSesionButton.isSelected {
                        
                        userDefaults.set(GlobalVariables.sharedManager.infoUsuarioBean.Usuario, forKey: defaultsKeys.USER_NAME_KEY)
                        userDefaults.set(GlobalVariables.sharedManager.infoUsuarioBean.UsuCla, forKey: defaultsKeys.USER_PASSWORD_KEY)
                        
                    }else{
                        userDefaults.set(nil, forKey: defaultsKeys.USER_NAME_KEY)
                        userDefaults.set(nil, forKey: defaultsKeys.USER_PASSWORD_KEY)
                    }
                    userDefaults.synchronize()
                    self.irHome()
                    
                }
            }else if (GlobalVariables.sharedManager.infoUsuarioBean.UseFlgClave == "N"){
            
                GlobalVariables.sharedManager.infoUsuarioBean.tipoAccesoCambiarContrasenia = "L"
                
                let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
                let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "CambiarContrasenaVC") as UIViewController
                self.present(vc, animated: false, completion: nil)
            }
        case "2":
            
            GlobalVariables.sharedManager.infoUsuarioBean.tipoAccesoCambiarContrasenia = "L"
            
            let mainStoryboard = UIStoryboard(name: "Login", bundle: Bundle.main)
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "CambiarContrasenaVC") as UIViewController
            self.present(vc, animated: false, completion: nil)
            
            
            
        default:
            print("otro")
        }
    }
    
    func irHome(){
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let menuLeftViewController = mainStoryboard.instantiateViewController(withIdentifier: "MenuLeftVC") as! MenuLeftViewController
        let homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
        menuLeftViewController.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        homeViewController.usuarioBean = GlobalVariables.sharedManager.infoUsuarioBean
        menuLeftViewController.setDefaultViewController(defaultViewController: homeViewController)
        let slideMenuController = ExSlideMenuController(mainViewController: homeViewController, leftMenuViewController: menuLeftViewController)
        
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        SlideMenuOptions.contentViewScale = 1
        
        self.navigationController!.pushViewController(slideMenuController, animated: true)
    }

}
