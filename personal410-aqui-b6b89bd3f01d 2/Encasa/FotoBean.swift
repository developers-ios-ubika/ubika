//
//  FotoBean.swift
//  Encasa
//
//  Created by Rommy Fuentes on 20/06/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//
import UIKit
class FotoBean: NSObject {
    var fotos = [FotoBean]()
    var codigo  : String? = ""
    var nombre  : String? = ""
    var descripcion  : String? = ""
    var obligatorio  : String? = ""
    var url  : String? = ""
    var foto:UIImage! = nil
    var sinDatos = false
    
}
