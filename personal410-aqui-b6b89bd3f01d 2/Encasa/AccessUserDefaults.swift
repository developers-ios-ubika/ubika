//
//  AccessUserDefaults.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 9/6/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class AccessUserDefaults {
    
    static func setUserDefaults(key: String, value: String)
    {
        // Store user data
        let userDefaults = UserDefaults.standard
        
        userDefaults.set(value, forKey: key)
        
        userDefaults.synchronize()
    }
    
    static func getUserDefaults(key: String) -> String
    {
        var value: String? = ""
        
        // Store user data
        let userDefaults = UserDefaults.standard
        
        value = userDefaults.string(forKey: key)
        
        if (value == nil)
        {
            value = ""
        }
        
        return value!
    }
    
    static func removeUserDefaults(key: String)
    {
        // Store user data
        let userDefaults = UserDefaults.standard
        
        userDefaults.removeObject(forKey: key)
        
        userDefaults.synchronize()
    }
}
