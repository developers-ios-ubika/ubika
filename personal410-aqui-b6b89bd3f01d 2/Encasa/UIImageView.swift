//
//  UIImageView.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 6/30/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

extension UIImageView {

    public func imageFromUrl(urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request){ data, response, error in
                self.image = UIImage(data: data!)
            }
            task.resume()
        }
    }
    
    func animationImageSecuence()
    {
        let arrayImage = [
            UIImage(named:"ic_audio1")!,
            UIImage(named:"ic_audio2")!,
            UIImage(named:"ic_audio3")!,
            ]
        
        self.animationImages = arrayImage
        self.animationDuration = 1.5
        self.startAnimating()
    }
    
    func animationImageSecuence(arrayImage: [UIImage])
    {
        self.animationImages = arrayImage
        self.animationDuration = 1
        self.startAnimating()
    }
}
