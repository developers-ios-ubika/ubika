//
//  CreateAccountViewController.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 25/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

class CreateAccountViewController: UIViewController, UITextViewDelegate{
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var containerPassView: UIView!
    @IBOutlet weak var containerConfirmPassView: UIView!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    @IBOutlet weak var politicasLabel: UITextView!
    @IBOutlet weak var otorgarLabel: UITextView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var infoTitleLabel: UILabel!
    @IBOutlet weak var descTitleLabel: UILabel!
    @IBOutlet weak var infoContainerView: UIView!
    @IBOutlet weak var checkPoliticasButton: UIButton!
    @IBOutlet weak var checkOtoButton: UIButton!
    
    private let politicasTyC = "He leído y acepto las Políticas de Privacidad de Mi Sentinel y además los Términos y Condiciones de Mi Sentinel."
    private let politicasBoldTyC = ["Políticas de Privacidad de Mi Sentinel", "Términos y Condiciones de Mi Sentinel."]
    private let otorgarTyC = "He leído y otorgo Consentimiento para Tratamiento de datos personales."
    private let otorgarBoldTyC = ["Tratamiento de datos personales."]
    private let infoTitle = "Mínimo 8 dígitos, mayúsculas, minúsculas,\nnúmero y algún símbolo (!@#$%&*)"
    private let descTitle = "No debe contener nombres o apellidos ni el\nnro. de documento."
    
    var dni: String!
    var resptTermin : RsptaTermyCResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getTermyConitions()
        initView()
        addStyleToElements()
    }
    
    //MARK: - Private methods
    private func initView() {
        
        politicasLabel.delegate = self
        otorgarLabel.delegate = self
        
        politicasLabel.attributedText = getAttributeDesc(text: politicasTyC, boldStrings: politicasBoldTyC, withLinks: politicasBoldTyC)
        otorgarLabel.attributedText = getAttributeDesc(text: otorgarTyC, boldStrings: otorgarBoldTyC, withLinks: otorgarBoldTyC)
        
        self.politicasLabel.linkTextAttributes = [.foregroundColor: Constants.Colors.colorPrimary]
        self.otorgarLabel
            .linkTextAttributes = [.foregroundColor: Constants.Colors.colorPrimary]
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(done))
        self.view.addGestureRecognizer(tapGesture)
        
        showInfoDesc(false)
    }
    
    @objc private func done(){
        view.endEditing(true)
    }
    
    private func addStyleToElements(){
        headerView.backgroundColor = Constants.Colors.grayHeader
        containerPassView.cutstomTextFieldsContainer(false)
        containerConfirmPassView.cutstomTextFieldsContainer(false)
        passTextField.isSecureTextEntry = true
        confirmPassTextField.isSecureTextEntry = true
        passTextField.delegate = self
        confirmPassTextField.delegate = self
        nextButton.personalizeButtonWithBorder(with: "Continuar", sizeFont: 14, withBold: true)
        
        infoTitleLabel.numberOfLines = 0
        infoTitleLabel.textAlignment = .left
        infoTitleLabel.textColor = .white
        
        descTitleLabel.numberOfLines = 0
        descTitleLabel.textAlignment = .left
        descTitleLabel.textColor = .white
        
        infoTitleLabel.setStyleFont(sizeFont: 10, withBold: true)
        descTitleLabel.setStyleFont(sizeFont: 10, withBold: true)
    }
    
    private func showInfoDesc(_ show: Bool){
        infoTitleLabel.text = show ? infoTitle : ""
        descTitleLabel.text = show ? descTitle : ""
        infoContainerView.cutstomInfoContainer()
        infoContainerView.isHidden = !show
    }
    
    private func getAttributeDesc(text: String, boldStrings: [String], withLinks: [String]) -> NSAttributedString {
        let description = NSMutableString(string: text)
        let attributeString = NSMutableAttributedString(string: text)
        attributeString.addAttribute(.font, value: UIFont(name: Constants.defualtFont, size: 11)!, range: NSRange(location: 0, length: attributeString.length))
        
        for boldString in boldStrings {
            for link in withLinks{
                let range : NSRange = description.range(of: boldString, options: .caseInsensitive)
                attributeString.addAttribute(.font, value: UIFont(name: Constants.defaultFontBold, size: 11)!, range: range)
                attributeString.addAttribute(.foregroundColor, value: Constants.Colors.colorPrimary, range: range)
                attributeString.addAttribute(.link, value: link, range: range)
            }
        }
        return attributeString
    }
    

    
    private func validatePassword() -> Bool{
        if self.passTextField.hasText && self.confirmPassTextField.hasText {
            if self.passTextField.text != self.confirmPassTextField.text{
                self.newAlert(titulo: "Login", mensaje: "Las contraseñas no coinciden", nameButton: "Terminar")
                return false
            }else{
                if passTextField.text!.isValidRequirementPassword() && confirmPassTextField.text!.isValidRequirementPassword(){
                    return true
                }
                self.newAlert(titulo: "Login", mensaje: "Contraseña inválida", nameButton: "Terminar")
                return false
            }
        }else{
            self.newAlert(titulo: "Login", mensaje: "Ingresa una contraseña", nameButton: "Terminar")
            return false
        }
    }
    
    private func createAccountPt1(){
        self.showActivityIndicator(uiView: self.view)
        self.view.isUserInteractionEnabled = false
        
        let tyc = self.checkPoliticasButton.isSelected ? "S" : "N"
        let tycOto = self.checkOtoButton.isSelected ? "S" : "N"
        let request = RegisterCreateRequest(Usuario: self.dni, ClaveInicial: self.passTextField.text, AceTyC: tyc, OtoCon: tycOto, TipoContr: 2, OrigenAplicacion: 11, VerPoli: "1", VerDatPer: "1")

        OriginData.sharedInstance.createAccountPt1( request: request){ (data) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            
            if data.code != "0"{
                self.newAlert(titulo: "Login", mensaje: data.mensaje ?? "", nameButton: "Terminar")
                return
            }
            
            let destinationVC = UIStoryboard.register.instantiate(ValidationIdentidadViewController.self)
            destinationVC.dni = self.dni
            destinationVC.pass = self.passTextField.text
            destinationVC.tyc = tyc
            destinationVC.tycOto = tycOto
            
            self.navigationController?.pushViewController(destinationVC, animated: true)

        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    private func getTermyConitions(){
        
        self.showActivityIndicator(uiView: self.view)
        self.view.isUserInteractionEnabled = false
        let request = TermyCRequest(Usuario: "", SesionId: "", origenAplicacion: 11)

        OriginData.sharedInstance.getTyC( request: request){ (data) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            
            if data.CodigoWS != "0"{
                self.newAlert(titulo: "Login", mensaje: "Error en el llamado de terminos y condiciones", nameButton: "Terminar")
                return
            }
            
            self.resptTermin = data
            self.politicasLabel.attributedText = self.getAttributeDesc(text: self.politicasTyC, boldStrings: self.politicasBoldTyC, withLinks: [self.resptTermin?.URLPoli ?? "", self.resptTermin?.URLTyC ?? ""])
            self.otorgarLabel.attributedText = self.getAttributeDesc(text: self.otorgarTyC, boldStrings: self.otorgarBoldTyC, withLinks: [self.resptTermin?.URLDatPer ?? ""])
            
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func showPassButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.tag == 0{
            passTextField.isSecureTextEntry = !sender.isSelected
        }else{
            confirmPassTextField.isSecureTextEntry = !sender.isSelected
        }
    }
    
    @IBAction func checkButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func infoButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        showInfoDesc(sender.isSelected)
    }
    
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if validatePassword(){
            if self.checkPoliticasButton.isSelected{
                self.createAccountPt1()
            }else{
                self.newAlert(titulo: "Login", mensaje: "Seleccione las Políticas de Privacidad", nameButton: "Terminar")
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension CreateAccountViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == passTextField && textField.hasText{
            containerPassView.cutstomTextFieldsContainer(true)
        }else if textField == passTextField && !textField.hasText{
            containerPassView.cutstomTextFieldsContainer(false)
        }
        if textField == confirmPassTextField && textField.hasText{
            containerConfirmPassView.cutstomTextFieldsContainer(true)
        }else if textField == confirmPassTextField && !textField.hasText{
            containerConfirmPassView.cutstomTextFieldsContainer(false)
        }
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return false
    }
}
