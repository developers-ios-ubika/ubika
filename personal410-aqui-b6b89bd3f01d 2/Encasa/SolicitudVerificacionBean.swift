//
//  SolicitudVerificacionBean.swift
//  Encasa
//
//  Created by Rommy Fuentes on 14/06/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

struct Response: Codable {
    var result: String
    var error: String
}

struct SolicitudVerificacionResponse: Codable {
    var result: [SolicitudVerificacion]
    var error: [ErrorResponse]
}

struct ErrorResponse: Codable {
    var code: String
    var message: String
}

struct SolicitudVerificacion: Codable {
    var solicitud: String
    var solicitudEmpresa: String
    var empresaTipDoc: String
    var empresaNroDoc: String
    var empresaRazonSocial  : String
    var logoUrl  : String
    var motivo  : Int?
    var direccion  : String
    var distrito  : String
    var departamento  : String
    var provincia  : String
    var pais  : String
    var horaMin  : String
    var horaMax  : String
    var fueraHorario  : Bool
    var cantidadHoras  : Int
    var cantidadMinutos : String?
    var ultimaOpcion  : Int
    var nroAlerta  : Int
    var fechaRegistro  : String
    var estado  : String?
    var usuario: UsuarioSolicitudVerificacion
    var configuracion: ConfiguracionSolicitudVerificacion
    var fotos: [FotosSolicitudVerificacion]
    
}

struct UsuarioSolicitudVerificacion: Codable {
    var direccionUsuario: String?
    var direccionUsuarioLat: String?
    var direccionUsuarioLon: String?
}

struct ConfiguracionSolicitudVerificacion: Codable{
    var intervaloAlerta: Int
    var tiempoEsperaAlerta: Int
    var frecuenciaControl: Int
    var radioControl: Int
}

struct FotosSolicitudVerificacion: Codable {
    var codigo: Int
    var nombre: String
    var descripcion: String
    var obligatorio: Bool
    var foto: Data?
    var sinDatos: Bool?
}

class SolicitudVerificacionBean: NSObject {

    var error = [ErrorBean]()
    
    var result = [SolicitudVerificacionBean]()
    
    var solicitud  : String? = ""
    var solicitudEmpresa  : String? = ""
    var empresaTipDoc  : String? = ""
    var empresaNroDoc  : String? = ""
    var empresaRazonSocial  : String? = ""
    var logoUrl  : String? = ""
    var motivo  : String? = ""
    var direccion  : String? = ""
    var distrito  : String? = ""
    var departamento  : String? = ""
    var provincia  : String? = ""
    var pais  : String? = ""
    var horaMin  : String? = ""
    var horaMax  : String? = ""
    var fueraHorario  : String? = ""
    var cantidadHoras  : String? = ""
    var cantidadMinutos : String? = ""
    var ultimaOpcion  : String? = ""
    var nroAlerta  : String? = ""
    var fechaRegistro  : String? = ""
    var estado  : String? = ""

    var usuario: UsuarioBean = UsuarioBean()
    //var usuarioBean: UsuarioBean = UsuarioBean()
    //var usuario : NSDictionary = NSDictionary()
    //var direccionUsuario  : String? = ""
    //var direccionUsuarioLat  : String? = ""
    //var direccionUsuarioLon  : String? = ""
    
    var configuracion: ConfiguracionBean = ConfiguracionBean()
    //var configuracionBean: ConfiguracionBean = ConfiguracionBean()
    //var configuracion : NSDictionary = NSDictionary()
    //var intervaloAlerta  : String? = ""
    //var tiempoEsperaAlerta  : String? = ""
    //var frecuenciaControl  : String? = ""
    //var radioControl  : String? = ""
    
    var fotos = [FotoBean]()
    
    var codigo  : String? = ""
    var descripcion  : String? = ""
    var obligatorio  : String? = ""
    var url  : String? = ""

    //var code  : String? = ""
    //var message  : String? = ""

    var cantSegundosAlerta  : String? = ""
}
