//
//  Constants.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 7/6/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

struct Constants {

    static let aplicacionNombre = "Aquí"
    static let defualtFont = "Helvetica"
    static let defaultFontBold = "Helvetica-Bold"
    
    struct Colors {
        static let colorPrimary = UIColor(red: 29.0/255.0, green: 79.0/255.0, blue: 145.0/255.0, alpha: 1.0)
        static let grayHeader = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
        static let grayTextField = UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1.0)
        static let grayInfoContainer = UIColor(red: 189.0/255.0, green: 189.0/255.0, blue: 189.0/255.0, alpha: 1.0)
        static let grayTextValidation = UIColor(red: 117.0/255.0, green: 117.0/255.0, blue: 117.0/255.0, alpha: 1.0)
    }
    
    
    typealias TermyConResp = (_ termyCond : RsptaTermyCResponse) -> Void
    typealias UserResp = (_ user : User) -> Void
    typealias WebSList = (_ list : WebServiceList) -> Void
    typealias RegisterDni = (_ dni : RegisterDniResponse) -> Void
    typealias MessageData = (_ dni : MessageDataResponse) -> Void
    typealias Questionaries = (_ questionaries : RegisterGenQuestionaryResponse) -> Void
    typealias RsptasQuestionary = (_ rsptas : RsptaCuestionaryResponse) -> Void
    typealias ErrorMessage = (_ error : ErrorResponse) -> Void
    typealias ErrorConnect = (_ noConnect : Bool) -> Void
    typealias AnyClassToken = (_ class : NSObject) -> Void
}
