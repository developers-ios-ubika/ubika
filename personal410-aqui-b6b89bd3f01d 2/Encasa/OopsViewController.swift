//
//  OopsViewController.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 26/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

enum errorEndQuestType: String {
    case none
    case novalido = "4"
    
    func getTitle() -> String? {
        switch self {
        case .novalido:
            return "Volver a intentar"
        default:
            return "Finalizar"
        }
    }
    
}
class OopsViewController: UIViewController{
    @IBOutlet weak var oopsTitleLabel: UILabel!
    @IBOutlet weak var oopsSubTitleLabel: UILabel!
    @IBOutlet weak var soporTitleLabel: UILabel!
    @IBOutlet weak var soporSubTitleLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var tryButton: UIButton!
    @IBOutlet weak var iconImageView: UIImageView!
    
    var rpsta: RsptaCuestionaryResponse!
    var error: errorEndQuestType = .novalido
    var isTimeOut: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        addStyleToElements()
    }
    
    //MARK: - Private methods
    private func initView() {
        
        oopsTitleLabel.text = "\(isTimeOut ? rpsta.DscError ?? "" : rpsta.MsgVal1 ?? "")\n\(rpsta.MsgVal2 ?? "")"
        oopsSubTitleLabel.text = isTimeOut ? "" : rpsta.MsgVal3
        self.tryButton.setTitle(self.error.getTitle(), for: .normal)
    }
    
    private func addStyleToElements(){
        oopsTitleLabel.numberOfLines = 0
        oopsTitleLabel.textAlignment = .center
        oopsTitleLabel.textColor = .black
        
        oopsTitleLabel.setStyleFont(sizeFont: 18, withBold: true)
        
        oopsSubTitleLabel.numberOfLines = 0
        oopsSubTitleLabel.textAlignment = .center
        oopsSubTitleLabel.textColor = Constants.Colors.grayTextValidation
        
        oopsSubTitleLabel.setStyleFont(sizeFont: 15, withBold: false)
        
        soporTitleLabel.numberOfLines = 0
        soporTitleLabel.textAlignment = .center
        soporTitleLabel.textColor = Constants.Colors.grayTextValidation
        
        soporTitleLabel.setStyleFont(sizeFont: 14, withBold: true)
        
        soporSubTitleLabel.numberOfLines = 0
        soporSubTitleLabel.textAlignment = .left
        soporSubTitleLabel.textColor = Constants.Colors.grayTextValidation
        
        soporSubTitleLabel.setStyleFont(sizeFont: 13, withBold: false)
        
        cancelButton.personalizeButtonSms(with: "Cancelar", sizeFont: 14, withBold: true)
        tryButton.personalizeButtonOrange(with: self.error == .none ? "Finalizar" : "Volver a intentar", sizeFont: 14, withBold: true)
        self.iconImageView.image = UIImage(named: self.isTimeOut ? "img_oops_time" : "img_oops")
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func netButtonPressed(_ sender: UIButton) {
        
        if sender.tag == 0{
            self.navigationController?.popToRootViewController(animated: true)
        }else{
            if self.error == .none{
                self.navigationController?.popToRootViewController(animated: true)
            }else{
                
                guard let viewControllers = self.navigationController?.viewControllers else {
                    self.navigationController?.popToRootViewController(animated: true)
                    return
                }

                for firstViewController in viewControllers {
                    if firstViewController is ValidationIdentidadViewController {
                        self.navigationController?.popToViewController(firstViewController, animated: true)
                        return
                    }
                }
                
            }
        }
    }

}
