//
//  ListaInicialBean.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 10/12/17.
//  Copyright © 2017 Sentinel. All rights reserved.
//

import UIKit

class ListaInicialBean: NSObject {
    
    var result = [ListaInicialBean]()
    var error : NSArray! = []
    
    var nombre : NSString = ""
    var url : NSString = ""
    
}

struct WebServiceList: Codable {
    var result: [WebServiceResult]
    var error: [String]
}

struct WebServiceResult: Codable {
    var nombre: String
    var url: String
}


