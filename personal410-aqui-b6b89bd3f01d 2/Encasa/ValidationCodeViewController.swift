//
//  ValidationCodeViewController.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 25/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

class ValidationCodeViewController: UIViewController{
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var containerPhoneView: UIView!
    @IBOutlet weak var containerCodeView: UIView!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var phoneTitleLabel: UILabel!
    @IBOutlet weak var codeTitleLabel: UILabel!
    @IBOutlet weak var sendSmsButton: UIButton!
    @IBOutlet weak var smsTitleLabel: UILabel!
    @IBOutlet weak var sectionPhoneView: UIView!
    @IBOutlet weak var sectionCodeView: UIView!
    
    @IBOutlet weak var topCodeContainerConstraint: NSLayoutConstraint!
    
    private let titlePhone = "Ingresa tu celular y te enviaremos un código\nvía SMS para poder validar tu registro."
    private let titleCode = "En caso de que no hayas recibido el mensaje\nde texto revisa tu correo electrónico."
    
    var dni: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        addStyleToElements()
    }
    
    //MARK: - Private methods
    private func initView() {
        
        phoneTitleLabel.text = titlePhone
        codeTitleLabel.text = titleCode
        smsTitleLabel.text = "Genera tu código vía:"
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(done))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func done(){
        view.endEditing(true)
    }
    
    private func addStyleToElements(){
        headerView.backgroundColor = Constants.Colors.grayHeader
        containerPhoneView.cutstomTextFieldsContainer(false)
        containerCodeView.cutstomTextFieldsContainer(false)
        
        phoneField.keyboardType = .numberPad
        codeTextField.keyboardType = .numberPad
        phoneField.delegate = self
        codeTextField.delegate = self
        
        phoneTitleLabel.numberOfLines = 0
        phoneTitleLabel.textAlignment = .center
        phoneTitleLabel.textColor = Constants.Colors.grayTextValidation
        
        codeTitleLabel.numberOfLines = 0
        codeTitleLabel.textAlignment = .center
        codeTitleLabel.textColor = Constants.Colors.grayTextValidation
        
        smsTitleLabel.numberOfLines = 0
        smsTitleLabel.textAlignment = .center
        smsTitleLabel.textColor = Constants.Colors.grayInfoContainer
        
        phoneTitleLabel.setStyleFont(sizeFont: 12, withBold: true)
        codeTitleLabel.setStyleFont(sizeFont: 12, withBold: true)
        smsTitleLabel.setStyleFont(sizeFont: 12, withBold: true)
        
        sendSmsButton.personalizeButtonSms(with: "SMS", sizeFont: 14, withBold: true)
        nextButton.personalizeButtonWithBorder(with: "Continuar", sizeFont: 14, withBold: true)

        sendSmsButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        
        sectionCodeView.isHidden = true
        topCodeContainerConstraint.constant = -sectionPhoneView.frame.height
        view.layoutIfNeeded()
    }

    private func validatePhone() -> Bool{
        if let text = self.phoneField.text, text.trim().count > 0{
            let result = text.trim().count == 9
            result ? print("phone Success") : self.newAlert(titulo: "Login", mensaje: "Ingresa un número de 9 digitos", nameButton: "Terminar")
            return result
        }else{
            self.newAlert(titulo: "Login", mensaje: "Ingresa un número celular", nameButton: "Terminar")
            return false
        }
    }
    
    private func validateCode() -> Bool{
        if let text = self.codeTextField.text, text.trim().count > 0{
            let result = text.trim().count == 3
            result ? print("code Success") : self.newAlert(titulo: "Login", mensaje: "Ingresa un cödigo de 3 digitos", nameButton: "Terminar")
            return result
        }else{
            self.newAlert(titulo: "Login", mensaje: "Ingresa un código", nameButton: "Terminar")
            return false
        }
    }
    
    private func sendSms(){
        
        self.showActivityIndicator(uiView: self.view)
        self.view.isUserInteractionEnabled = false
        let request = RegisterSmsRequest(Usuario: self.dni, Celular: self.phoneField.text, OrigenAplicacion: 11)

        OriginData.sharedInstance.sendSms( request: request){ (data) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            if data.code != "0"{
                self.newAlert(titulo: "Login", mensaje: data.mensaje ?? "", nameButton: "Terminar")
                return
            }
            print("revisar SMS")
            
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.view.isUserInteractionEnabled = true
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    private func confirmSms(){
        
        self.showActivityIndicator(uiView: self.view)
        self.view.isUserInteractionEnabled = false
        let request = RegisterConfirmSmsRequest(Usuario: self.dni, ClaveSMS: self.codeTextField.text, OrigenAplicacion: 11)

        OriginData.sharedInstance.sendConfirmSms( request: request){ (data) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            
            if data.code != "0"{
                self.newAlert(titulo: "Login", mensaje: data.mensaje ?? "", nameButton: "Terminar")
                return
            }
            
            let destinationVC = UIStoryboard.register.instantiate(CreateAccountViewController.self)
            destinationVC.dni = self.dni
            self.navigationController?.pushViewController(destinationVC, animated: true)

        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
            self.view.isUserInteractionEnabled = true
        }
    }
    
    // MARK: - IBAction Methods
    @IBAction func generateButtonPressed(_ sender: UIButton) {
        if validatePhone(){
            sectionCodeView.isHidden = false
            topCodeContainerConstraint.constant = 0
            view.layoutIfNeeded()
            sendSms()
        }
    }
    
    @IBAction func netButtonPressed(_ sender: UIButton) {
        if validateCode(){
            self.confirmSms()
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ValidationCodeViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        return count <= (textField == phoneField ? 9 : 3)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == phoneField && textField.hasText{
            containerPhoneView.cutstomTextFieldsContainer(true)
        }else if textField == phoneField && !textField.hasText{
            containerPhoneView.cutstomTextFieldsContainer(false)
        }
        if textField == codeTextField && textField.hasText{
            containerCodeView.cutstomTextFieldsContainer(true)
        }else if textField == codeTextField && !textField.hasText{
            containerCodeView.cutstomTextFieldsContainer(false)
        }
    }
}
