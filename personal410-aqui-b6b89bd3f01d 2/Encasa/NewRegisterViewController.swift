//
//  NewRegisterViewController.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 25/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

class NewRegisterViewController: UIViewController{
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var containerDniView: UIView!
    @IBOutlet weak var containerEmailView: UIView!
    @IBOutlet weak var containerCodeView: UIView!
    @IBOutlet weak var dniField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameUserLabel: UILabel!
    @IBOutlet weak var containerNameUserView: UIView!
    @IBOutlet weak var containerImageVerifView: UIView!
    @IBOutlet weak var topNmaeUserContainerConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleDigitLabel: UILabel!
    @IBOutlet weak var subTitleDigitLabel: UILabel!
    @IBOutlet weak var descDigitLabel: UIButton!
    @IBOutlet weak var isRegisterLabel: UILabel!
    @IBOutlet weak var loginHereLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        addStyleToElements()
    }
    
    //MARK: - Private methods
    private func initView() {
        
        titleLabel.text = "¡Hola! Sigue estos simples pasos para\nregistrarte y acceder a Ubika"
        nameUserLabel.text = "Luis Carlos Montoya Salazar"
        titleDigitLabel.text = "Dígito verificador:"
        subTitleDigitLabel.text = "Por tu seguridad el sistema se bloqueará al tercer intento fallido y deberás ponerte en contacto con nosotros para continuar."
        isRegisterLabel.text = "¿Ya estás registrado?"
        loginHereLabel.text = "Inicia sesión aquí!"
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(done))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func done(){
        view.endEditing(true)
    }
    
    private func addStyleToElements(){
        headerView.backgroundColor = Constants.Colors.grayHeader
        
        containerImageVerifView.cutstomContainerRoundHeader()
        containerNameUserView.cutstomInfoContainer()
        
        containerDniView.cutstomTextFieldsContainer(false)
        containerEmailView.cutstomTextFieldsContainer(false)
        containerCodeView.cutstomTextFieldsContainer(false)
        
        dniField.keyboardType = .numberPad
        emailTextField.keyboardType = .default
        codeTextField.keyboardType = .numberPad
        
        dniField.delegate = self
        emailTextField.delegate = self
        codeTextField.delegate = self
        
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = Constants.Colors.grayTextValidation
        
        titleDigitLabel.numberOfLines = 0
        titleDigitLabel.textAlignment = .left
        titleDigitLabel.textColor = Constants.Colors.grayTextValidation
        
        subTitleDigitLabel.numberOfLines = 0
        subTitleDigitLabel.textAlignment = .left
        subTitleDigitLabel.textColor = Constants.Colors.grayTextValidation
        
        isRegisterLabel.numberOfLines = 0
        isRegisterLabel.textAlignment = .center
        isRegisterLabel.textColor = Constants.Colors.grayTextValidation
        
        loginHereLabel.numberOfLines = 0
        loginHereLabel.textAlignment = .center
        loginHereLabel.textColor = Constants.Colors.colorPrimary
        
        nameUserLabel.textColor = .white
        descDigitLabel.isUserInteractionEnabled = false
        
        titleLabel.setStyleFont(sizeFont: 12, withBold: true)
        nameUserLabel.setStyleFont(sizeFont: 11, withBold: true)
        titleDigitLabel.setStyleFont(sizeFont: 12, withBold: true)
        subTitleDigitLabel.setStyleFont(sizeFont: 10, withBold: false)
        isRegisterLabel.setStyleFont(sizeFont: 10, withBold: false)
        loginHereLabel.setStyleFont(sizeFont: 10, withBold: false)
        
        descDigitLabel.personalizeButtonTelef(with: "(511) 514 9000", sizeFont: 11, withBold: true)
        nextButton.personalizeButtonWithBorder(with: "Continuar", sizeFont: 14, withBold: true)
        
        containerNameUserView.isHidden = true
        topNmaeUserContainerConstraint.constant = -containerDniView.frame.height
        view.layoutIfNeeded()
    }

    private func validateDni() -> Bool{
        if let text = self.dniField.text, text.trim().count > 0{
            let result = text.trim().count == 8
            result ? print("phone Success") : self.newAlert(titulo: "Login", mensaje: "Ingresa un número de 8 digitos", nameButton: "Terminar")
            return result
        }else{
            self.newAlert(titulo: "Login", mensaje: "Ingrese su número de DNI.", nameButton: "Terminar")
            return false
        }
    }
    
    private func validateCode() -> Bool{
        if let text = self.codeTextField.text, text.trim().count > 0{
            let result = text.trim().count == 1
            result ? print("code Success") : self.newAlert(titulo: "Login", mensaje: "Ingresa código verificador", nameButton: "Terminar")
            return result
        }else{
            self.newAlert(titulo: "Login", mensaje: "Ingresa código verificador", nameButton: "Terminar")
            return false
        }
    }
    
    private func isEmailValid() -> Bool {
        if !emailTextField.hasText {
            self.newAlert(titulo: "Login", mensaje: "Ingresa correo", nameButton: "Terminar")
            return false
        }
        
        if emailTextField.isFirstResponder {
            emailTextField.resignFirstResponder()
        }
        
        if emailTextField.text!.validateFormatEmail(){
            return true
        } else {
            self.newAlert(titulo: "Login", mensaje: "Correo inválido", nameButton: "Terminar")
            return false
        }
    }
    
    private func loadDni(){
        
        self.showActivityIndicator(uiView: self.view)
        
        let request = RegisterDniRequest(Usuario: self.dniField.text, OrigenAplicacion: 11)

        OriginData.sharedInstance.getNewDni( request: request){ (dni) in
            self.hideActivityIndicator(uiView: self.view)
            if dni.codigows != "0"{
                self.showAlert(titulo: "Login", mensaje: "\(dni.mensaje ?? "")")
                return
            }
            self.nameUserLabel.text = "\(dni.nombres ?? "") \(dni.apepat ?? "") \(dni.apemat ?? "")".capitalized
            self.containerNameUserView.isHidden = false
            self.topNmaeUserContainerConstraint.constant = 20
            self.view.layoutIfNeeded()
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    private func sendRegister(){
        
        self.showActivityIndicator(uiView: self.view)
        
        let request = RegisterDataRequest(Usuario: self.dniField.text, Mail: self.emailTextField.text, DigitoVer: self.codeTextField.text, CodigoRef: "", OrigenAplicacion: 11)

        OriginData.sharedInstance.registerData( request: request){ (data) in
            self.hideActivityIndicator(uiView: self.view)
            if data.code != "0"{
                self.newAlert(titulo: "Login", mensaje: data.mensaje ?? "", nameButton: "Terminar")
                return
            }
            
            let destinationVC = UIStoryboard.register.instantiate(ValidationCodeViewController.self)
            destinationVC.dni = self.dniField.text
            self.navigationController?.pushViewController(destinationVC, animated: true)
            
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    // MARK: - IBAction Methods

    @IBAction func netButtonPressed(_ sender: UIButton) {
        
        if validateDni() && isEmailValid() && validateCode(){
            sendRegister()
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NewRegisterViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if textField == dniField && count <= 8{
            return true
        }else if textField == emailTextField && count <= 100{
            return true
        }else if textField == codeTextField && count <= 1{
            return true
        }else{
            return false
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dniField{
            containerNameUserView.isHidden = true
            topNmaeUserContainerConstraint.constant = -containerDniView.frame.height
            view.layoutIfNeeded()
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == dniField && textField.hasText{
            if textField.text?.trim().count == 8{
                self.loadDni()
            }
            containerDniView.cutstomTextFieldsContainer(true)
        }else if textField == dniField && !textField.hasText{
            containerDniView.cutstomTextFieldsContainer(false)
        }
        if textField == emailTextField && textField.hasText{
            containerEmailView.cutstomTextFieldsContainer(true)
        }else if textField == emailTextField && !textField.hasText{
            containerEmailView.cutstomTextFieldsContainer(false)
        }
        if textField == codeTextField && textField.hasText{
            containerCodeView.cutstomTextFieldsContainer(true)
        }else if textField == codeTextField && !textField.hasText{
            containerCodeView.cutstomTextFieldsContainer(false)
        }
    }
}
