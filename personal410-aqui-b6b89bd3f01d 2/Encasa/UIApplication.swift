//
//  UIApplication.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/22/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

extension UIApplication {
    
    class func topViewController(viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = viewController as? UINavigationController {
            return topViewController(viewController: nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(viewController: selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(viewController: presented)
        }
        
        if let slide = viewController as? SlideMenuController {
            return topViewController(viewController: slide.mainViewController)
        }
        return viewController
    }
    

}
