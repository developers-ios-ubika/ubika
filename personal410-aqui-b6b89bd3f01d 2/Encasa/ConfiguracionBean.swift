//
//  ConfiguracionBean.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 6/27/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class ConfiguracionBean: NSObject {

    var intervaloAlerta : String! = ""
    var tiempoEsperaAlerta  : String! = ""
    var frecuenciaControl  : String! = ""
    var radioControl  : String! = ""
}
