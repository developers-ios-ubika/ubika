//
//  ValidationIdentidadViewController.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 25/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

class ValidationIdentidadViewController: UIViewController{
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    private let titles = "Te haremos unas preguntas para\nvalidar tu identidad y proceder\ncon el registro"
    private let subTitle = "Para tu tranquilidad, esta información es\nconfidencial y no será compartida\na terceros."
    private let desc = "Recuerda que tienes 3 intentos."
    
    var dni: String!
    var pass: String!
    var tyc: String!
    var tycOto: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        addStyleToElements()
    }
    
    //MARK: - Private methods
    private func initView() {
        
        titleLabel.text = titles
        subTitleLabel.text = subTitle
        descLabel.text = desc
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(done))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func done(){
        view.endEditing(true)
    }
    
    private func addStyleToElements(){
        headerView.backgroundColor = Constants.Colors.grayHeader

        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = Constants.Colors.grayTextValidation
        
        titleLabel.setStyleFont(sizeFont: 12, withBold: false)

        subTitleLabel.numberOfLines = 0
        subTitleLabel.textAlignment = .center
        subTitleLabel.textColor = Constants.Colors.grayTextValidation
        
        subTitleLabel.setStyleFont(sizeFont: 12, withBold: false)
        
        descLabel.numberOfLines = 0
        descLabel.textAlignment = .center
        descLabel.textColor = Constants.Colors.grayTextValidation
        
        descLabel.setStyleFont(sizeFont: 12, withBold: false)
        
        nextButton.personalizeButtonWithBorder(with: "Continuar", sizeFont: 14, withBold: true)
    }
    
    private func getQuestionaries(){
        self.showActivityIndicator(uiView: self.view)
        let request = RegisterGenCuestionaryRequest(Usuario: dni, origenAplicacion: 11, TipoDocumento: "D", NroDocumento: dni)

        OriginData.sharedInstance.getQuestionaries( request: request){ (data) in
            self.hideActivityIndicator(uiView: self.view)

            if data.resulviso != 0{
                self.newAlert(titulo: "Login", mensaje: data.dscresulviso ?? "", nameButton: "Terminar")
                return
            }
            
            let destinationVC = UIStoryboard.register.instantiate(QuestionaryViewController.self)
            destinationVC.dni = self.dni
            destinationVC.pass = self.pass
            destinationVC.tyc = self.tyc
            destinationVC.tycOto = self.tycOto
            destinationVC.questionaryResponse = data
            
            self.navigationController?.pushViewController(destinationVC, animated: true)
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    // MARK: - IBAction Methods
    
    @IBAction func netButtonPressed(_ sender: UIButton) {
        getQuestionaries()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
