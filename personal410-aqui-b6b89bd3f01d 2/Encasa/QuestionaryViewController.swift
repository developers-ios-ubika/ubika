//
//  QuestionaryViewController.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 26/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

class QuestionaryViewController: UIViewController{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    
    var seconds = 0
    var timer = Timer()
    
    var questionaryResponse: RegisterGenQuestionaryResponse?{
        didSet{
            self.seconds = questionaryResponse?.questionary?.timeEva ?? 60
        }
    }
    
    var dni: String!
    var pass: String!
    var tyc: String!
    var tycOto: String!
    var arrayResptas = [RsptaCuestionaryRequest]()
    
    private func sendQuestionary(isTime: Bool){

        if isTime{
            if let _ = arrayResptas.first(where: {$0.rptaId == ""}){
                for (index, _) in arrayResptas.enumerated(){
                    arrayResptas[index].rptaId = ""
                }
            }
        }
        
        self.showActivityIndicator(uiView: self.view)
        
        let request = ValidateCuestionaryRequest(Usuario: dni, origenAplicacion: 11, TipoDocumento: "D", NroDocumento: dni, CodCue: questionaryResponse?.codcue, CodEvaluacion: Int(questionaryResponse?.questionary?.codevaluation ?? "0"), PreRpta: arrayResptas)
        
        OriginData.sharedInstance.sendQuestionary( request: request){ (data) in
            self.hideActivityIndicator(uiView: self.view)
            
            if data.CodigoWS != "0"{
                self.newAlert(titulo: "Login", mensaje: data.DscError ?? "", nameButton: "Terminar")
                return
            }
            
            if data.Estado?.lowercased() == "aprobado"{
                let destinationVC = UIStoryboard.register.instantiate(SuccessQuestionViewController.self)
                destinationVC.dni = self.dni
                destinationVC.pass = self.pass
                destinationVC.tyc = self.tyc
                destinationVC.tycOto = self.tycOto
                self.navigationController?.pushViewController(destinationVC, animated: true)
            }else{
                let destinationVC = UIStoryboard.register.instantiate(OopsViewController.self)
                destinationVC.rpsta = data
                destinationVC.isTimeOut = data.Estado?.lowercased() == "sin respuesta"
                if data.NroIntDisp == 0{
                    destinationVC.error = .none
                }else{
                    destinationVC.error = .novalido
                }
                
                self.navigationController?.pushViewController(destinationVC, animated: true)
            }
            
        } andError: { (errorResponse) in
            self.hideActivityIndicator(uiView: self.view)
            self.showAlert(titulo: "\(errorResponse.code)", mensaje: errorResponse.message)
        } andErrorConnect: { (errorConnect) in
            self.hideActivityIndicator(uiView: self.view)
        }
    }
    
    func createTempRespts(){
        arrayResptas = []
        if let questions = self.questionaryResponse?.questionary?.questions{
            for quest in questions {
                arrayResptas.append(RsptaCuestionaryRequest(preguntaId: "\(quest.codpregunta ?? 0)", rptaId: ""))
            }
        }
        tableView.reloadData()
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    func resetTimer(){
        self.seconds = questionaryResponse?.questionary?.timeEva ?? 60
        timerLabel.text = "Cuentas con \(timeString(time: TimeInterval(seconds))) minutos para responder"
        self.timer.invalidate()
    }
    
    @objc func updateTimer(){
        if seconds < 1 {
            sendQuestionary(isTime: true)
            timer.invalidate()
            
        } else {
            seconds -= 1
            timerLabel.text = "Cuentas con \(timeString(time: TimeInterval(seconds))) minutos para responder"
        }
    }
    
    func runTimer() {
        timerLabel.text = "Cuentas con \(timeString(time: TimeInterval(seconds))) minutos para responder"
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(updateTimer)),
                                     userInfo: nil, repeats: true)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.runTimer()
        self.createTempRespts()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        resetTimer()
    }
    
    //MARK: - Private methods
    private func initView() {
        configureTableView()
    
        timerLabel.numberOfLines = 0
        timerLabel.textAlignment = .center
        nextButton.personalizeButtonWithBorder(with: "Continuar", sizeFont: 14, withBold: true)
    }
    
    private func configureTableView() {
        let bundle = Bundle(for: ValidationIdentidadTableViewCell.self)
        let nib = UINib(nibName: "ValidationIdentidadTableViewCell", bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: "ValidationIdentidadTableViewCell")
        tableView.separatorStyle = .none
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedSectionHeaderHeight = 45
        tableView.rowHeight = 45
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 60, right: 0)
    }
    
    // MARK: - IBAction Methods
    
    @IBAction func netButtonPressed(_ sender: UIButton) {
        if self.arrayResptas.first(where: {$0.rptaId == ""}) == nil{
            self.sendQuestionary(isTime: false)
        }else{
            newAlert(titulo: "Login", mensaje: "Selecciona todas las opciones", nameButton: "Terminar")
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension QuestionaryViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return questionaryResponse?.questionary?.questions?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionaryResponse?.questionary?.questions?[section].alternativas?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ValidationIdentidadTableViewCell", for: indexPath) as! ValidationIdentidadTableViewCell
        if let alternative = questionaryResponse?.questionary?.questions?[indexPath.section].alternativas?[indexPath.row]{
            cell.delegate = self
            cell.index = indexPath
            cell.selectCheck(isSelect: alternative.isCheck)
            cell.loadData(title: alternative.desalternative ?? "")
            let index = tableView.numberOfRows(inSection: indexPath.section)
            cell.isLastView(isLast: indexPath.row != index - 1)
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .white
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = Constants.Colors.grayTextValidation
        label.setStyleFont(sizeFont: 12, withBold: true)
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        label.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        label.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        label.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        var title = "\(section + 1) \(questionaryResponse?.questionary?.questions?[section].despregunta ?? "")"
        title = title.replacingOccurrences(of: "<B>", with: "")
        title = title.replacingOccurrences(of: "</B>", with: "")
        label.text = title.capitalized
        view.addSubview(label)
        return view
    }
}

extension QuestionaryViewController: ValidationIdentidadDelegate{
    func check(cell: ValidationIdentidadTableViewCell, index: IndexPath) {
        if let questions = questionaryResponse?.questionary?.questions?[index.section],
           let alternativa = questionaryResponse?.questionary?.questions?[index.section].alternativas?[index.row],
           let alternativas = questions.alternativas{
            for alt in alternativas{
                alt.isCheck = false
            }
            alternativa.isCheck = !alternativa.isCheck
            
            self.arrayResptas[index.section].preguntaId = "\(questions.codpregunta ?? 0)"
            self.arrayResptas[index.section].rptaId = "\(alternativa.codalternative ?? 0)"
        }
        self.tableView.reloadData()
    }
}

