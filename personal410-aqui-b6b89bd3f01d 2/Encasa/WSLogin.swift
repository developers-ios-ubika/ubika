//
//  WSLogin.swift
//  SentinelTuit
//
//  Created by Juan Alberto Carlos Vera on 3/23/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class WSLogin: WSParent {

    static let sharedInstance = WSLogin()
    
    static func validarLogin(notificacion: String, parametros: NSDictionary){
        
        //self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "iniciarSesion", clase: UsuarioBean.self)
        let loginRequest = LoginRequest(Usuario: parametros["Usuario"] as! String, UsuCla: parametros["UsuCla"] as! String, Origen: parametros["Origen"] as! String, Originado: parametros["Originado"] as! String)
        self.validateLogin(notification: notificacion, parametros: loginRequest, metodo: "iniciarSesion")
    }
    
    static func obtenerLoginDetalle(notificacion: String, parametros: NSDictionary){
        let request = UserRequest(Usuario : parametros["Usuario"] as! String,
                                  SesionId: parametros["SesionId"] as! String)
        self.getUser(notification: notificacion, parametros: request, metodo: "obtenerUsuario")
        //self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "obtenerUsuario", clase: UsuarioBean.self)
    }
    static func solicitarCambiarClave(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "nuevaClave", clase: UsuarioBean.self)
    }
    static func cambiarClave(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "cambiarClave", clase: UsuarioBean.self)
    }
    static func consultarServicio(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ServUsuario", clase: UsuarioBean.self)
    }
    static func consultaDatosServicio(notificacion: String, parametros: NSDictionary){
    
    self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWSDatosServicio", clase: DatoServicioBean.self)
    }
    static func setearToken(notificacion: String, parametros: NSDictionary){
        
        //self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ActTokenUsu", clase: NotificationPush.self)
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ActTokenUsuAPP", clase: NotificationPush.self)
    }
    
    // Ultimos web services
    
    static func actualizarToken(notificacion: String, parametros: NSDictionary){
        
        //self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ActTokenUsu", clase: NotificationPush.self)
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "actualizarTokenApp", clase: NotificationPush.self)
    }
    
    static func setToken(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_RegistraToken", clase: NotificationPush.self)
    }
    
    static func cargarMisServicios(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_CargaCombo", clase: NotificationPush.self)
    }

    static func validarDNI(notificacion: String, parametros: NSDictionary){
    
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarDNI", clase: UsuarioBean.self)
    }
    static func validarMail(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarCorreo", clase: UsuarioBean.self)
    }

    static func validarCelular(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarCelular", clase: UsuarioBean.self)
    }
    static func generarClaveSMS(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "generarClaveSMS", clase: UsuarioBean.self)
    }

    static func generarPassword(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "generarClave", clase: UsuarioBean.self)
    }
    static func validarCodigo(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarEnvioSMS", clase: UsuarioBean.self)
    }

    static func generarAFPInsInvA(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "registrarConsultaGratuita", clase: UsuarioBean.self)
    }

    static func AceptaContratoUsu(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "registroContrato", clase: UsuarioBean.self)
    }
    static func aceptarContrato(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "aceptarContrato2", clase: UsuarioBean.self)
    }

    static func AFPActRegYa(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "aceptarContrato", clase: UsuarioBean.self)
    }

    static func obtenerMensaje(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "RWS_ObtErrorWS", clase: ErrorBean.self)
    }
    //===========
    static func listarSolicitudVerificacion(notificacion: String, parametros: NSDictionary){
        let request = SolicitudesRequest(sesionId: parametros["sesionId"] as! String, plataforma: parametros["plataforma"] as! String, usuario: parametros["usuario"] as! String)
        self.getRequestsList(notification: notificacion, parametros: request, metodo: "obtenerListaSolicitud")
//        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "obtenerListaSolicitud", clase: SolicitudVerificacionBean.self)
    }

    static func obtenerSolicitudVerificacion(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "obtenerSolicitud", clase: SolicitudVerificacionBean.self)
    }
    
    static func ingresarDireccion(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "insertarDireccion", clase: ResultadoBean.self)
    }
    static func validarDireccion(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarDirecciones", clase: ResultadoBean.self)
    }
    
    static func insertarEvento(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "insertarEvento", clase: ResultadoBean.self)
    }
    
    static func validarVigenciaSolicitud(notificacion: String, parametros: NSDictionary){
        
        self.prepareSessionDataTask(notificacion: notificacion, parametros: parametros, metodo: "validarVigenciaSolicitud", clase: ResultadoBean.self)
    }
    
    static func cargaListaWS(notification: String, parametros: NSDictionary){
        //self.prepareSessionDataTask(notificacion: notification, parametros: parametros, metodo: "RWS_VD_listaWebService", clase: ListaInicialBean.self)
        let list = ServiceListRequest()
        self.getServiceList(notificacion: notification, parametros: list, metodo: "RWS_VD_listaWebService")
    }
  
}

// Se agrega extension para separar el response del servicio actual
extension WSLogin{
    static func cargaNewListaWS(withSuccess success: @escaping Constants.WebSList, andError errorMesg: @escaping Constants.ErrorMessage){
        let list = ServiceListRequest()
        self.getNewServiceList(parametros: list, withSuccess: success, andError: errorMesg)
    }
    
    static func validarNewLoginWS(parametros: NSDictionary, withSuccess success: @escaping Constants.UserResp, andError errorMesg: @escaping Constants.ErrorMessage){
        let loginRequest = LoginRequest(Usuario: parametros["Usuario"] as! String, UsuCla: parametros["UsuCla"] as! String, Origen: parametros["Origen"] as! String, Originado: parametros["Originado"] as! String)
        self.validateNewLogin(parametros: loginRequest, metodo: "iniciarSesion", withSuccess: success, andError: errorMesg)
    }
    
    static func obtenerNewLoginDetalle(parametros: NSDictionary, withSuccess success: @escaping Constants.UserResp, andError errorMesg: @escaping Constants.ErrorMessage){
        let request = UserRequest(Usuario : parametros["Usuario"] as! String,
                                  SesionId: parametros["SesionId"] as! String)
        self.getNewUser(parametros: request, metodo: "obtenerUsuario", withSuccess: success, andError: errorMesg)
    }
    
    static func actualizarNewToken(nparametros: NSDictionary, withSuccess success: @escaping Constants.AnyClassToken){
   
        self.prepareNewSessionDataTask(parametros: nparametros, metodo: "actualizarTokenApp", clase: NotificationPush.self, withSuccess: success)
    }
    
    static func getNewDniWS(request: RegisterDniRequest, withSuccess success: @escaping Constants.RegisterDni, andError errorMesg: @escaping Constants.ErrorMessage){
        self.getNewDni(parametros: request, withSuccess: success, andError: errorMesg)
    }
    
    static func registerDataWS(request: RegisterDataRequest, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        self.registerData(parametros: request, withSuccess: success, andError: errorMesg)
    }
    
    static func sendSmsWS(request: RegisterSmsRequest, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        self.sendSms(parametros: request, withSuccess: success, andError: errorMesg)
    }
    
    static func sendConfirmSmsWS(request: RegisterConfirmSmsRequest, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        self.sendConfirmSms(parametros: request, withSuccess: success, andError: errorMesg)
    }
    
    static func createAccountPt1WS(request: RegisterCreateRequest, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        self.createAccountPt1(parametros: request, withSuccess: success, andError: errorMesg)
    }
    
    static func createAccountPt2WS(request: RegisterCreateRequest, withSuccess success: @escaping Constants.MessageData, andError errorMesg: @escaping Constants.ErrorMessage){
        self.createAccountPt2(parametros: request, withSuccess: success, andError: errorMesg)
    }
    
    static func getQuestionariesWS(request: RegisterGenCuestionaryRequest, withSuccess success: @escaping Constants.Questionaries, andError errorMesg: @escaping Constants.ErrorMessage){
        self.getQuestionaries(parametros: request, withSuccess: success, andError: errorMesg)
    }
    
    static func sendQuestionaryWS(request: ValidateCuestionaryRequest, withSuccess success: @escaping Constants.RsptasQuestionary, andError errorMesg: @escaping Constants.ErrorMessage){
        self.sendQuestionary(parametros: request, withSuccess: success, andError: errorMesg)
    }
    
    static func getTyCWS(request: TermyCRequest, withSuccess success: @escaping Constants.TermyConResp, andError errorMesg: @escaping Constants.ErrorMessage){
        self.getTyC(parametros: request, withSuccess: success, andError: errorMesg)
    }
}

