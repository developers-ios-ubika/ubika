import Foundation
class MCEpicToken:NSObject {
    override init() {
        super.init()
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0]
        let rutaPlist:String = "\(paths)/token.plist"
        let exist = FileManager.default.fileExists(atPath: rutaPlist)
        if (!exist){
            let dic = NSDictionary(dictionary: ["token": "", "isInServer": "0"])
            dic.write(toFile: rutaPlist as String, atomically: true)
        }
    }
    func setToken(token:UnsafeMutablePointer<String>){
        token.pointee = self.returnCleanToken(token: token.pointee)
        let dic = NSDictionary(dictionary: ["token": token, "isInServer": "0"])
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0]
        let rutaPlist:NSString = paths.appending("/token.plist") as NSString
        dic.write(toFile: rutaPlist as String, atomically: true)
    }
    func getToken()->String{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as NSString
        let rutaPlist:NSString = paths.appending("/token.plist") as NSString
        let dict = NSDictionary(contentsOfFile: rutaPlist as String)
        let token = dict!.object(forKey: "token")! as! String
        return token
    }
    func isInServer()->Bool{
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as NSString
        let rutaPlist:NSString = paths.appending("/token.plist") as NSString
        let dict = NSDictionary(contentsOfFile: rutaPlist as String)
        let token = dict!.object(forKey: "isInServer")! as! String
        if(token == "0"){
            return false
        }
        return false
    }
    func inServer(inServer:Bool){
        var value = ""
        if(inServer){
            value = "1";
        }
        let token = getToken()
        let dic = NSDictionary(dictionary: ["token": token, "isInServer": value])
        let rutaPlist = Bundle.main.path(forResource: "Property", ofType: "plist")
        dic.write(toFile: rutaPlist!, atomically: true)
    }
    func returnCleanToken (token : String)->String{
        var cleanToken = "Big red car"
        cleanToken = (token as NSString).replacingOccurrences(of: "<", with: "")
        cleanToken = (cleanToken as NSString).replacingOccurrences(of: ">", with: "")
        cleanToken = (cleanToken as NSString).replacingOccurrences(of: "<", with: "")
        return cleanToken
    }
}
