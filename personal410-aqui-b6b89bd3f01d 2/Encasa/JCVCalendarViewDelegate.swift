//
//  JCVCalendarViewDelegate.swift
//  Tuit-Inves
//
//  Created by Juan Alberto Carlos Vera on 5/15/16.
//  Copyright © 2016 Orbita. All rights reserved.
//

import UIKit

protocol JCVCalendarViewDelegate1 {

    func didSelectDay()
}
