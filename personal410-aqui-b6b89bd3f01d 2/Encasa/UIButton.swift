//
//  UIButton.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 23/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

extension UIButton {
    
    func personalizeButton(title : String, sizeFont : CGFloat, withBold: Bool) {
        setTitle(title, for: .normal)
        titleLabel?.font = UIFont(name: withBold ? Constants.defaultFontBold : Constants.defualtFont, size: sizeFont)
    }
    
    func personalizeButtonWithBorder(with title: String, sizeFont: CGFloat, withBold: Bool) {
        personalizeButton(title: title, sizeFont: sizeFont, withBold: withBold)
        
        self.layer.borderColor = Constants.Colors.colorPrimary.cgColor
        self.layer.borderWidth = 1.5
        self.layer.cornerRadius = 4.0
        self.backgroundColor = .white
        self.setTitleColor(Constants.Colors.colorPrimary, for: .normal)
    }
    
    func personalizeButtonGrayWithBorder(with title: String, sizeFont: CGFloat, withBold: Bool) {
        personalizeButton(title: title, sizeFont: sizeFont, withBold: withBold)
        
        self.layer.borderColor = Constants.Colors.grayTextValidation.cgColor
        self.layer.borderWidth = 1.5
        self.layer.cornerRadius = 4.0
        self.backgroundColor = .clear
        self.setTitleColor(Constants.Colors.grayTextValidation, for: .normal)
    }
    
    func personalizeButtonSms(with title: String, sizeFont: CGFloat, withBold: Bool) {
        personalizeButton(title: title, sizeFont: sizeFont, withBold: withBold)
        
        self.layer.cornerRadius = 4.0
        self.backgroundColor = Constants.Colors.grayInfoContainer
        self.setTitleColor(.white, for: .normal)
    }
    
    func personalizeButtonTelef(with title: String, sizeFont: CGFloat, withBold: Bool) {
        personalizeButton(title: title, sizeFont: sizeFont, withBold: withBold)
        self.setTitleColor( Constants.Colors.grayTextValidation, for: .normal)
    }
    
    func personalizeButtonOrange(with title: String, sizeFont: CGFloat, withBold: Bool) {
        personalizeButton(title: title, sizeFont: sizeFont, withBold: withBold)
        
        self.layer.cornerRadius = 4.0
        self.backgroundColor = Constants.Colors.colorPrimary//.orange
        self.setTitleColor(.white, for: .normal)
    }
}
