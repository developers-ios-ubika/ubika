//
//  BorderButton.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 28/03/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class BorderButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = layer.bounds.height/2
        
    }

}
