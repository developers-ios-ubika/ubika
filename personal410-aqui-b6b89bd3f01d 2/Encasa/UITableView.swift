//
//  UITableView.swift
//  Encasa
//
//  Created by Juan Alberto Carlos Vera on 9/5/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

extension UITableView {
/*
    func roundCorners(corners: UIRectCorner, radius: CGFloat){
        
        let bounds = self.bounds
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(radius, radius))
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.CGPath
        self.layer.mask = maskLayer
        
        let frameLayer = CAShapeLayer()
        frameLayer.frame = bounds
        frameLayer.path = maskPath.CGPath
        frameLayer.strokeColor = UIColor.fromHex(rgbValue: 0xb3b3b3).cgColor
        frameLayer.fillColor = nil
        
        self.layer.addSublayer(frameLayer)
    }

    
     func setBorder(){
     
     self.layer.borderColor = UIColor.fromHex(rgbValue: 0xEAEAEA).cgColor
     self.layer.borderWidth = 2.0
     }
     
    func setShadow()
    {
        let containerView:UIView = UIView(frame:self.frame)
        //dont use clear color,fit blue color
        containerView.backgroundColor = UIColor.blue
        //shadow view also need cornerRadius
        containerView.layer.cornerRadius = 10
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOffset = CGSizeMake(-10, 10); //Left-Bottom shadow
        //containerView.layer.shadowOffset = CGSizeMake(10, 10); //Right-Bottom shadow
        containerView.layer.shadowOpacity = 1.0
        containerView.layer.shadowRadius = 2
        
        self.addSubview(containerView)
    }
    
    func setRoundCorners(){
        
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }
    */
}
