//
//  CircularBorderLabel.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 26/04/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit

class CircularBorderLabel: UILabel {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = layer.bounds.height/2
        
    }
}
