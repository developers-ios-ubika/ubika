//
//  WhiteBorderButton.swift
//  SentinelTuit
//
//  Created by Rommy Fuentes on 23/03/16.
//  Copyright © 2016 Sentinel. All rights reserved.
//

import UIKit


class WhiteBorderButton: UIButton {

    let borderAlpha : CGFloat = 1
    let cornerRadius : CGFloat = 20.0
    let greenColor = UIColor.fromHex(rgbValue: 0x1E6734,alpha:0.9).cgColor
    let whiteColor = UIColor.fromHex(rgbValue: 0xFFFFFF).cgColor
    var statusButton = "0"
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = cornerRadius
        layer.borderWidth = 2.0
        layer.borderColor = greenColor
        
    }
    
    // MARK: - target action methods
    
//    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        
//        if(statusButton == "0"){
//            layer.borderColor = whiteColor
//            statusButton = "1"
//        }else{
//            layer.borderColor = greenColor
//            statusButton = "0"
//
//        }
//        
//    }
    
}

