//
//  RegisterDni.swift
//  Encasa
//
//  Created by Walter Alonso Rodriguez Castañeda on 25/11/20.
//  Copyright © 2020 Sentinel. All rights reserved.
//

import UIKit

struct MessageDataResponse: Codable{
    let mensaje: String?
    let code: String?
    
    enum CodingKeys: String, CodingKey {
        case mensaje = "MensajeRespuesta"
        case code = "CodigoWS"
    }
}

struct RegisterDniResponse: Codable{
    let apepat: String?
    let apemat: String?
    let nombres: String?
    let mensaje: String?
    let codigows: String?
    
    enum CodingKeys: String, CodingKey {
        case apepat = "ApePat"
        case apemat = "ApeMat"
        case nombres = "Nombres"
        case mensaje = "MensajeRespuesta"
        case codigows = "CodigoWS"
    }
}

struct RegisterGenQuestionaryResponse: Codable{
    let resulviso: Int?
    let dscresulviso: String?
    let codcue: Int?
    let code: String?
    let questionary: QuestionaryResponse?
    
    enum CodingKeys: String, CodingKey {
        case resulviso = "ResulVISO"
        case dscresulviso = "DscResulVISO"
        case codcue = "CodCue"
        case code = "CodigoWS"
        case questionary = "Cuestionario"
    }
}

class QuestionaryResponse: Codable{
    let codevaluation: String?
    let dateIni: String?
    let timeEva: Int?
    let questions: [QuestionResponse]?
    
    enum CodingKeys: String, CodingKey {
        case codevaluation = "CodEvaluacion"
        case dateIni = "FechaHoraInicio"
        case timeEva = "TiempoEvaluacion"
        case questions = "Preguntas"
    }
}

class QuestionResponse: Codable{
    let codpregunta: Int?
    let despregunta: String?
    let alternativas: [AlternativeResponse]?
    
    enum CodingKeys: String, CodingKey {
        case codpregunta = "CodPregunta"
        case despregunta = "DesPregunta"
        case alternativas = "Alternativas"
    }
}

class AlternativeResponse: Codable{
    let codalternative: Int?
    let desalternative: String?
    var isCheck = false
    
    enum CodingKeys: String, CodingKey {
        case codalternative = "CodAlternativa"
        case desalternative = "DesAlternativa"
    }
}

struct RegisterDniRequest: Codable {
    let Usuario: String?
    let OrigenAplicacion: Int?
}

struct RegisterDataRequest: Codable {
    let Usuario: String?
    let Mail: String?
    let DigitoVer: String?
    let CodigoRef: String?
    let OrigenAplicacion: Int?
}

struct RegisterSmsRequest: Codable{
    let Usuario: String?
    let Celular: String?
    let OrigenAplicacion: Int?
}

struct RegisterConfirmSmsRequest: Codable{
    let Usuario: String?
    let ClaveSMS: String?
    let OrigenAplicacion: Int?
}

struct RegisterCreateRequest: Codable{
    let Usuario: String?
    let ClaveInicial: String?
    let AceTyC: String?
    let OtoCon: String?
    let TipoContr: Int?
    let OrigenAplicacion: Int?
    let VerPoli: String?
    let VerDatPer: String?
}

struct RegisterGenCuestionaryRequest: Codable{
    let Usuario: String?
    let origenAplicacion: Int?
    let TipoDocumento: String?
    let NroDocumento: String?
}

struct ValidateCuestionaryRequest: Codable{
    let Usuario: String?
    let origenAplicacion: Int?
    let TipoDocumento: String?
    let NroDocumento: String?
    let CodCue: Int?
    let CodEvaluacion: Int?
    let PreRpta: [RsptaCuestionaryRequest]?
    
}

struct RsptaCuestionaryRequest: Codable{
    var preguntaId: String?
    var rptaId: String?
}

struct RsptaCuestionaryResponse: Codable{
    let Estado: String?
    let CodError: String?
    let DscError: String?
    let CodEvaFin: String?
    let NroIntDisp: Int?
    let MsgVal1: String?
    let MsgVal2: String?
    let MsgVal3: String?
    let CodigoWS: String?
}

struct TermyCRequest: Codable{
    let Usuario: String?
    let SesionId: String?
    let origenAplicacion: Int?
}

struct RsptaTermyCResponse: Codable{
    let URLTyC: String?
    let URLPoli: String?
    let URLDatPer: String?
    let CodigoWS: String?
}
